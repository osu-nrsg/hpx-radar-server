/**
 * @file SpinLock.hpp
 * @brief Contains the SpinLock class.
 * @copyright Copyright (C) 2009-2019, Panagiotis Christopoulos Charitos and contributors.
 * All rights reserved.
 * Code licensed under the BSD License.
 * http://www.anki3d.org/LICENSE
 */
#include <atomic>

/**
 * Simple spinlock class, extracted from anki-3d-engine.
 *
 * https://github.com/godlikepanos/anki-3d-engine/blob/a2ea83a52a5dd7660458f7db650b48cb4e921394/src/anki/util/Thread.h/
 * (BSD Licensed)
 */
class SpinLock {
 public:
  void lock() {
    while(lck.test_and_set(std::memory_order_acquire)) {}
  }
  void unlock() {
    lck.clear(std::memory_order_release);
  }
 private:
  std::atomic_flag lck = ATOMIC_FLAG_INIT;
};
