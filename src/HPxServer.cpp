/**
 * @file HPxServer.cpp
 * @brief Implementation of HPxServer class and a few helper functions.
 *
 * @date Sep 26, 2018
 * @author Randall Pittman <Randall.Pittman@oregonstate.edu>
 * @copyright MIT (see LICENSE.txt)
 */

// Class definition header
#include "HPxServer.hpp"
// --Includes not in the class definition header--
// C/C++ std
#include <chrono>
#include <map>
#include <memory>
#include <thread>
#include <cassert>
#include <cstdio>
#include <csignal>
#include <ctime>
// 3rd-party
#include <cpptoml.h>
#include <boost/date_time/posix_time/posix_time.hpp>

// Using declarations
using boost::posix_time::ptime;
using std::string;
using std::to_string;
using std::difftime;
using std::time_t;
using std::time;
using std::chrono::steady_clock;
using std::chrono::milliseconds;
using std::make_unique;

/// Period to sleep each loop of the statemachine
constexpr milliseconds statemachine_sleep_ms(10);
/// Minimum time after alarm to clear alarm
constexpr milliseconds alarm_timeout(5000);

// called by signal
volatile sig_atomic_t kill_signal = 0;
volatile sig_atomic_t hup_signal = 0;

/// Callback to handle OS signals to shut down the server (SIGTERM & SIGINT)
static void handle_kill_signal(int sig) {
  kill_signal = sig;
}

/// Callback to handle SIGHUP
static void handle_hup_signal(int sig) {
  hup_signal = sig;
}

/**
 * Set SPx library features and return a pointer to a new SPxHPx100Source object.
 *
 * Set the log file and the debug flags.
 * @param spx_log_file  FILE* to write SPx library log info
 * @param debug_flags   Debug flags for SPx library. See SetDebug() in manual.
 * @return Pointer to new SPxHPx100Source object.
 */
static SPxHPx100Source* init_SPx_src(FILE* spx_log_file, UINT32 debug_flags) {
  SPxHPx100Source::SetLogFile(spx_log_file);
  SPxHPx100Source::SetDebug(debug_flags);
  return new SPxHPx100Source(NULL);
}

HPxServer::HPxServer(const string& publisher_endpoint, const string& hpx_conf_path,
                     const string& config_toml, bool daemon_mode, UINT32 spx_debug_flags)
    : hpx_src(init_SPx_src(spx_log_pipe.fp_write, spx_debug_flags)),
      config_path(hpx_conf_path),
      config_toml(config_toml),
      pub_endpoint(publisher_endpoint),
      context(1),
      info_pub(open_pub_socket(context, proxy_endpoint, false)),
      zmq_proxy(new ZmqProxy(context, proxy_endpoint, pub_endpoint)) {
  init_daemon_mode(daemon_mode);
  // Wait for proxy to be running, thow an exception if there's a proxy error.
  do {
    std::this_thread::sleep_for(std::chrono::milliseconds(100));
  } while (!zmq_proxy->is_running() && !zmq_proxy->eptr);
  if (zmq_proxy->eptr) {
    std::rethrow_exception(zmq_proxy->eptr);
  }
  register_signal_handlers();

  // Make sure if it's a unix socket to give group r/w permissions
  update_socket_perms(pub_endpoint);

  capture_spx_log(spx_log_pipe, loglevel::info);  // emit anything currently in SPx lib log to app log

  // Set up the SPx Error Handler instance
  spxerr_handler_ptr = make_unique<SPxErrorHandler>();
  // Install the function that will be run if the SPx library has an error.
  SPxSetErrorHandler(handle_spx_errors, spxerr_handler_ptr.get());
}

void HPxServer::register_signal_handlers() {
  // Install handlers for OS signals.
  struct sigaction kill_action;
  struct sigaction hup_action;
  memset(&kill_action, 0, sizeof(kill_action));
  memset(&hup_action, 0, sizeof(hup_action));
  kill_action.sa_handler = handle_kill_signal;
  hup_action.sa_handler = handle_hup_signal;
  sigaction(SIGINT, &kill_action, NULL);
  sigaction(SIGTERM, &kill_action, NULL);
  sigaction(SIGHUP, &hup_action, NULL);
}

HPxServer::~HPxServer() {
  notify_shutdown();
  // Stop the HPx board and ray handler and close the HPx board.
  stop();
  // Destroy the ray handler and spx error handler objects. This closes their sockets too.
  ray_handler_ptr.reset(nullptr);
  spxerr_handler_ptr.reset(nullptr);
  // Close the sockets in the ray publisher and the status/configure pub socket.
  info_pub.close();
  // Stop and destroy the ZmqProxy
  zmq_proxy.reset(nullptr);
  // Close the context.
  context.close();
  notify_shutdown_done();
  remove_socket(pub_endpoint);
  return;
}

void HPxServer::run_server() {
  // Set up periods to send watchdog signal and transmit STATUS & CONFIG messages
  milliseconds watchdog_period(hpx_watchdog_us/1000/2);
  SPDLOG_TRACE("Watchdog period ms: {}", watchdog_period.count());
  milliseconds status_config_period(hpx_config.status_and_config_period_ms);
  SPDLOG_TRACE("Status/Config period ms: {}", status_config_period.count());

  // Set up the state queue for the state machine
  StateQueue stateq;
  stateq.push(ServerState::begin);
  ServerState last_state = stateq.front();

  // Get times to next transmit watchdog and status & config
  auto next_config_status_time = steady_clock::now();
  auto next_watchdog_notify_time = steady_clock::now();

  // State machine.
  // Every loop consumes a state from the queue and thus the queue must not be allowed to become empty.
  // `stopping` is the only state that does not push another state onto the queue; thus any
  // states that push `stopping` onto the queue must also push another state to go to after `stopping`.
  while (stateq.front() != ServerState::end) {
    current_state = stateq.front();
    if (last_state != current_state) {
      SPDLOG_TRACE("State transition: '{}' to '{}'", state_string(last_state), state_string(current_state));
    }
    capture_spx_log(spx_log_pipe, loglevel::info);  // Emit anything from SPx lib log to app log
    // switch statement determines actions and next state
    switch (current_state) {
      case ServerState::begin:
        // Always configure and start the server immediately upon launch
        stateq.push(ServerState::config_and_start);
        break;
      case ServerState::config_and_start:
        // transition state
        // Try to configure and start the ray handler and SPx source
        assert (!is_running(last_state));  // illogical to configure when running
        if (configure_and_start() == 0) {
          stateq.push(ServerState::running);
        } else {
          stateq.push(ServerState::stopped_config_error);
        }
        break;
      case ServerState::stopping:
        assert (!is_stopped(last_state));  // illogical to stop when stopped already
        stop();
        break;
      case ServerState::running:
      case ServerState::running_alarm:
      case ServerState::stopped:
      case ServerState::stopped_config_error:
      case ServerState::stopped_run_error:
        // These are the "non-transitory" states. get_next_run_states() determines whether to continue as-is
        // or switch states.
        get_next_run_states(stateq, last_state);
        break;
      case ServerState::end:
        assert(false);  // while loop should prevent ever reaching here.
      default:
        assert(false);  // Should never get here unless missing a state case or `break` statement.
    }
    if (steady_clock::now() >= next_config_status_time) {
      publish_config();
      publish_status(current_state);
      next_config_status_time += status_config_period;
    }
    if (watchdog_period > milliseconds(0) && steady_clock::now() >= next_watchdog_notify_time) {
      notify_watchdog();
      next_watchdog_notify_time += watchdog_period;
    }
    last_state = current_state;
    stateq.pop();
    assert(!stateq.empty());  // State queue should never run out of states.
    std::this_thread::sleep_for(statemachine_sleep_ms);
  }
  // Publish one last status before leaving.
  publish_status(stateq.front());
}

int HPxServer::configure_and_start() {
  notify_starting();
  if (get_config() != 0) {
    return -1;
  }
  if (prepare_for_daq() != 0) {
    return -2;
  }
  if (start() != 0) {
    return -3;
  }
  notify_running();
  return 0;
}

void HPxServer::get_next_run_states(StateQueue& stateq, ServerState last_state) {
  static steady_clock::time_point last_alarm_tp;  // Epoch (1970-01-01)
  auto dt_since_alarm = steady_clock::now() - last_alarm_tp;
  std::map<int, string> sig_names;
  sig_names[SIGINT] = "SIGINT";
  sig_names[SIGTERM] = "SIGTERM";
  sig_names[SIGHUP] = "SIGHUP";

  // First, check for any thread exceptions. If there are any, move to run_error state.
  if (check_thread_exceptions() != 0) {
    stateq.push(ServerState::stopping);
    stateq.push(ServerState::stopped_run_error);
    return;
  }
  // No thread exceptions, so keep going. Check for any terminate or control signals.
  int signal = check_signals();
  bool quit = false;
  bool reload = false;
  bool alarm = false;
  switch (signal) {
    case 0:
      // no signal, no new states (keep going)
      break;
    case SIGINT: // SIGINT or SIGTERM or non-daemon SIGHUP
    case SIGTERM:
      quit = true;
      break;
    case SIGHUP:
      if (hpx_daemon_mode) reload = true;
      else quit = true;
      break;
    default:
      throw HPxServerError("Unexpected return from check_signals: " + to_string(signal));
  }
  alarm = ((current_state == ServerState::running || current_state == ServerState::running_alarm) &&
           check_alarms() != 0);
  if (quit) {
      SPDLOG_DEBUG("Received {} - exiting", sig_names[signal]);
      stateq.push(ServerState::stopping);
      stateq.push(ServerState::end);
  } else if (reload) {
      SPDLOG_DEBUG("Received reset signal (SIGHUP)");
      stateq.push(ServerState::stopping);
      stateq.push(ServerState::config_and_start);
      notify_reloading();
  } else if (alarm) {
    stateq.push(ServerState::running_alarm);
    last_alarm_tp = steady_clock::now();
  } else if (last_state == ServerState::running_alarm && dt_since_alarm > alarm_timeout) {
    // Alarm has cleared.
    SPDLOG_INFO("{} ms since last alarm. Alarms cleared.", alarm_timeout.count());
    stateq.push(ServerState::running);
  } else {
    // Continue with the current state.
    stateq.push(current_state);
  }
  return;
}

int HPxServer::get_config() {
  SPDLOG_INFO("--LOADING CONFIGURATION--");
  std::string test_path_etc = "/etc/hpx-radar-server.toml";
  if (config_path.empty() && fexist(test_path_etc)) {
      config_path = test_path_etc;
  }
  if (config_path != "") {
    SPDLOG_INFO("Using config file at {}", config_path);
  } else {
    SPDLOG_ERROR("No configuration supplied or available at {}", test_path_etc);
    return -1;
  }
  if (config_toml != "") {
    SPDLOG_INFO("Using additional config params \"{}\"", config_toml);
  }
  hpx_config = HPxConfig(config_path, config_toml);
  hpx_config.log(loglevel::debug);
  // If initial config is no good, then the server can't be started.
  config_load_err = "";
  if (!hpx_config.is_valid()) {
    config_load_err = hpx_config.invalid_reason();
    SPDLOG_ERROR("Cannot start server due to bad configure: " + hpx_config.invalid_reason());
    return -1;
  }
  return 0;
}

int HPxServer::prepare_for_daq() {
  SPDLOG_INFO("--OPENING AND CONFIGURING DAQ BOARD--");
  // This must only be run with a valid config.
  if (!hpx_config.is_valid()) {
    throw HPxServerError("Cannot configure the board if the config is not valid.");
  }

  // Configure the board for DAQ.
  board_config_err = "";
  board_ready = false;
  board_configured = false;
  try {
    open_and_configure_board();
    // completed config with no exceptions
    board_configured = true;
  } catch (...) {
    // Try to close the board (but don't bother doing anything if that fails) and send an error
    // message.
    if (hpx_src->IsBoardOpen()) {
      hpx_src->CloseBoard();
    }
    board_config_err = "Error configuring the HPx board: " + what(std::current_exception());
    return -1; // don't set up ray handler on error
  }

  // Set up the Ray Handler instance and install the ray handler function.
  // Note that reset() calls the destructor of any previous ray handler.
  try {
    ray_handler_ptr.reset(new RayHandler(hpx_src.get(), hpx_config, context, proxy_endpoint));
    // IDEA: change ray_handler_ptr to shared_ptr and pass a weak_ptr to return_handler_fn?
    // Install the function that will be run for every ray (AKA return)
    hpx_src->InstallDataFn(return_handler_fn, ray_handler_ptr.get());
    board_ready = true;
  } catch (...) {
    if (hpx_src->IsBoardOpen()) {
      hpx_src->CloseBoard();
    }
    board_config_err = "Error config_and_run ray handler: " + what(std::current_exception());
    return -1;
  }
  return 0;
}

void HPxServer::open_and_configure_board() {
  // Open the board.
  if (SPxErrorCode err_code = hpx_src->OpenBoard(hpx_config.board_idx)) {
    throw HPxSPxError(err_code, "OpenBoard failed.");
  }
  SPDLOG_INFO("Opened " + (string)hpx_src->GetCardName() +
              " card index " + to_string(hpx_config.board_idx) +
              " with FPGA " + hexstr(hpx_src->GetFpgaVersion()));

  // Set up the video channel.
  // Set the video channel based on the configured analog channel. Always do 12-bit.
  if (hpx_config.analog_channel == 'A') {
    if (SPxErrorCode err_code = hpx_src->SetVideoChannel(SPX_HPX200_VIDEO_A_12BIT)) {
      throw HPxSPxError(err_code, "Failed to set video channel.");
    }
    if (SPxErrorCode err_code = hpx_src->SetOffsetA(hpx_config.voltage_offset)) {
      throw HPxSPxError(err_code, "Failed to set voltage offset.");
    }
    if (SPxErrorCode err_code = hpx_src->SetGainA(hpx_config.gain)) {
      throw HPxSPxError(err_code, "Failed to set gain.");
    }
  } else if (hpx_config.analog_channel == 'B') {
    if (SPxErrorCode err_code = hpx_src->SetVideoChannel(SPX_HPX200_VIDEO_B_12BIT)) {
      throw HPxSPxError(err_code, "Failed to set video channel.");
    }
    if (SPxErrorCode err_code = hpx_src->SetOffsetA(hpx_config.voltage_offset)) {
      throw HPxSPxError(err_code, "Failed to set voltage offset.");
    }
    if (SPxErrorCode err_code = hpx_src->SetGainB(hpx_config.gain)) {
      throw HPxSPxError(err_code, "Failed to set gain.");
    }
  } else {
    SPDLOG_WARN("Analog channel should be 'A' or 'B'. No channel selected.");
  }

  // Configure sampling.
  if (SPxErrorCode err_code = hpx_src->SetNumSamples(hpx_config.num_samples)) {
    throw HPxSPxError(err_code, "Failed to set number of samples.");
  }
  if (SPxErrorCode err_code = hpx_src->SetStartRangeMetres(hpx_config.start_range_metres)) {
    throw HPxSPxError(err_code, "Failed to set start range.");
  }
  // This calculation seems to keep the same range bins on both HPx-200 and HPx-410 cards.
  double endMetres = hpx_src->GetStartRangeMetres() + 2.997*hpx_config.num_samples - 1.5;
  if (SPxErrorCode err_code = hpx_src->SetEndRangeMetres(endMetres)) {
    throw HPxSPxError(err_code, "Failed to set end range.");
  }
  if (SPxErrorCode err_code =
      hpx_src->SetRangeCorrectionMetres(hpx_config.range_correction_metres)) {
    throw HPxSPxError(err_code, "Failed to set range correction (trigger delay).");
  }

  // Configure the TPG.
  if (hpx_config.tpg_enabled) {
    unsigned int tpg = SPX_HPX100_TPG_DISABLE;
    switch (hpx_config.tpg_mode) {
      case TpgMode::analog_ramp:
        tpg = (hpx_config.analog_channel == 'A') ? SPX_HPX100_TPG_ANALOGUE_A : SPX_HPX100_TPG_ANALOGUE_B;
        break;
      case TpgMode::digital_pulse:
        tpg = SPX_HPX100_TPG_DIG_PULS_PULS;
        break;
      case TpgMode::digital_ramp:
        tpg = SPX_HPX100_TPG_DIG_RAMP_RAMP;
        break;
      case TpgMode::offset_dac:
        tpg = SPX_HPX100_TPG_DAC;
        break;
    }

    if (SPxErrorCode err_code = hpx_src->SetTPG(tpg)) {
      throw HPxSPxError(err_code, "Failed to set test pattern generator.");
    }
    if (SPxErrorCode err_code = hpx_src->SetTPGtrgFreq(hpx_config.tpg_trigger_frequency)) {
      throw HPxSPxError(err_code, "Failed to set test pattern generator trigger freq.");
    }
    if (SPxErrorCode err_code = hpx_src->SetTPGacpFreq(hpx_config.tpg_acp_frequency)) {
      throw HPxSPxError(err_code, "Failed to set test pattern generator ACP freq.");
    }
    if (SPxErrorCode err_code = hpx_src->SetTPGarpFreq(hpx_config.tpg_arp_frequency)) {
      throw HPxSPxError(err_code, "Failed to set test pattern generator ARP freq.");
    }
  } else {
    if (SPxErrorCode err_code = hpx_src->SetTPG(SPX_HPX100_TPG_DISABLE)) {
      throw HPxSPxError(err_code, "Failed to set test pattern generator to disabled.");
    }
  }

  // Configure azimuth interpolation and bank interrupt settings.
  if (SPxErrorCode err_code =
      hpx_src->SetAziInterpolation((int)hpx_config.enable_azimuth_interpolation)) {
    throw HPxSPxError(err_code, "Failed to set azimuth interpolation.");
  }
  if (hpx_config.spokes_per_interrupt > 0) {
    // Only set if non-negative. Note--have to reset the board to revert this to "unset".
    if (SPxErrorCode err_code =
        hpx_src->SetSpokesPerInterrupt(hpx_config.spokes_per_interrupt)) {
      throw HPxSPxError(err_code, "Failed to set number of spokes per interrupt.");
    }
  }
  if (hpx_config.interrupts_per_second > 0) {
    if (SPxErrorCode err_code =
        hpx_src->SetInterruptsPerSecond(hpx_config.interrupts_per_second)) {
      throw HPxSPxError(err_code, "Failed to set number of interrupts per second.");
    }
  }

  // Report some settings for debug verification.
  SPDLOG_DEBUG("HPx Card config completed successfully. Config confirmation:");
  SPDLOG_DEBUG("Num Samples: " + to_string(hpx_src->GetNumSamples()));
  SPDLOG_DEBUG("Req Start Range Metres: " + to_string(hpx_src->GetRequestedStartRangeMetres()));
  SPDLOG_DEBUG("Start Range Metres: " + to_string(hpx_src->GetStartRangeMetres()));
  SPDLOG_DEBUG("Req End Range Metres: " + to_string(hpx_src->GetRequestedEndRangeMetres()));
  SPDLOG_DEBUG("End Range Metres: " + to_string(hpx_src->GetEndRangeMetres()));
  SPDLOG_DEBUG("Range Correction Metres: " + to_string(hpx_src->GetRangeCorrectionMetres()));
  SPDLOG_DEBUG("Channel A Voltage Offset: " + to_string(hpx_src->GetOffsetA()));
  SPDLOG_DEBUG("Spokes Per Interrupt: " + to_string(hpx_src->GetSpokesPerInterrupt()));
}

int HPxServer::start() {
  SPDLOG_INFO("--STARTING SERVER--");
  // This must only be run with a configured and ready board.
  if (!board_configured || !board_ready) {
    throw HPxServerError("Cannot start until board is configured and ready.");
  }
  try {
    // Start up the ray handler consumer thread and the SPx ray handler.
    ray_handler_ptr->run();
    time(&tstart);  // reset the run_server timer
    hpx_src->Enable(TRUE);
  } catch (...) {
    if (hpx_src->IsBoardOpen()) {
      hpx_src->CloseBoard();
    }
    board_config_err = "Error starting ray handler or enabling DAQ card: " + what(std::current_exception());
    return -1;
  }
  return 0;
}

void HPxServer::stop() {
  // Make sure the board and ray handling is stopped.
  if (hpx_src->GetEnable() == TRUE) {
    SPDLOG_INFO("--STOPPING SERVER--");
    hpx_src->Enable(FALSE);
  }
  if (ray_handler_ptr) {
    ray_handler_ptr->stop();
  }
  if (hpx_src->IsBoardOpen()) {
    hpx_src->CloseBoard();
  }
}

int HPxServer::check_signals() {
  int signal = 0;
  if (kill_signal) signal = kill_signal;
  else if (hup_signal) signal = hup_signal;
  kill_signal = 0;
  hup_signal = 0;
  return signal;
}

int HPxServer::check_thread_exceptions() {
  // Check for exceptions in the threads we manage and report any, if found.
  string exg_err = run_err;  // save the current error message.
  run_err = "";
  if (eptrs.spx_thread) {
    run_err += "Ray handler err: " + what(eptrs.spx_thread);
  }
  if (eptrs.consumer_thread) {
    if (run_err != "") run_err += "\n";
    run_err += "Ray handler consumer err: " + what(eptrs.consumer_thread);
  }
  if (eptrs.spxerr_thread) {
    if (run_err != "") run_err += "\n";
    run_err += "SPx err: " + what(eptrs.spxerr_thread);
  }
  if (run_err == "") {
    // If no new error message, restore the last one.
    run_err = exg_err;
  }

  // Reset exception pointers, then return whether there were any exceptions.
  int retval = 0;
  if (eptrs.any()) {
    retval = -1;
  }
  eptrs.spx_thread = nullptr;
  eptrs.consumer_thread = nullptr;
  eptrs.spxerr_thread = nullptr;
  return retval;
}

int HPxServer::check_alarms() {
  constexpr unsigned int OK = TRUE;
  constexpr unsigned int MISSING = FALSE;
  unsigned int trig_status, acp_status, arp_status;
  static unsigned int last_trig_status = OK;
  static unsigned int last_acp_status = OK;
  static unsigned int last_arp_status = OK;
  int retval = 0;
  alarm_msg = "";
  hpx_src->GetSignalStatus(&trig_status, &acp_status, &arp_status);
  if (trig_status == MISSING) {
    if (last_trig_status == OK) {
      SPDLOG_WARN("TRIGGER ALARM");
    }
    alarm_msg = "TRIGGER ALARM";
    retval |= 1;
  }
  if (acp_status == MISSING) {
    if (last_acp_status == OK) {
      SPDLOG_WARN("ACP ALARM");
    }
    if (alarm_msg != "") alarm_msg += ", ";
    alarm_msg += "ACP ALARM";
    retval |= 2;
  }
  if (arp_status == MISSING) {
    if (last_arp_status == OK) {
      SPDLOG_WARN("ARP ALARM");
    }
    if (alarm_msg != "") alarm_msg += ", ";
    alarm_msg += "ARP ALARM";
    retval |= 4;
  }
  last_trig_status = trig_status;
  last_acp_status = acp_status;
  last_arp_status = arp_status;
  return retval;
}

void HPxServer::publish_config() {
  static zmq::message_t body_msg;
  // Pack the config message, push it into the 0MQ message, and send it.
  hpx_config.pack(*hpx_src);
  body_msg.rebuild(hpx_config.sbuf.data(), hpx_config.sbuf.size());
  zmq_send_envelope(info_pub, "CONFIG", body_msg);
}


void HPxServer::publish_status(const ServerState& server_state) {
  // Get the current time for diff with start time, create an HPxStatus message, and publish
  // it to the 0MQ publisher with the envelope tag 'STATUS'.
  static zmq::message_t body_msg;
  string message;
  int runtime_s = 0;

  if (server_state == ServerState::running || server_state == ServerState::running_alarm) {
    runtime_s = difftime(time(0), tstart);
  }

  // Populate the message field based on any errors.
  switch (server_state) {
    case ServerState::stopped_config_error:
      if (config_load_err != "") {
        message = config_load_err;
      } else {
        message = board_config_err;
      }
      break;
    case ServerState::stopped_run_error:
      message = run_err;
      break;
    case ServerState::running_alarm:
      message = alarm_msg;
      break;
    default:
      message = "";
  }

  // Pack the status message, push it into the 0MQ message, and send it.
  int rays_last_rot = -1;
  if (ray_handler_ptr) {
    rays_last_rot = ray_handler_ptr->rays_last_rot;
  }
  hpx_status.pack(server_state, *hpx_src, runtime_s, rays_last_rot, message);
  body_msg.rebuild(hpx_status.sbuf.data(), hpx_status.sbuf.size());
  zmq_send_envelope(info_pub, "STATUS", body_msg);

  // Log status.
  SPDLOG_DEBUG(
      "STATUS: {} | PRF: {:.1f}, meas_azimuths: {:d}, meas_rays: {:d}, runtime_s: {:d}, message: {}",
      state_string(hpx_status.state),
      hpx_status.avg_prf,
      hpx_status.meas_azimuths,
      hpx_status.meas_rays,
      hpx_status.runtime_s,
      hpx_status.message);
}

