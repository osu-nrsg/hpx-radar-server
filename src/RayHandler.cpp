/**
 * @file RayHandler2.cpp
 * @brief Implementation of RayHandler2 class and helper functions.
 *
 * @date Nov 1, 2019
 * @author Randall Pittman <Randall.Pittman@oregonstate.edu>
 * @copyright MIT (see LICENSE.txt)
 */

#include <algorithm>  // std::transform
#include <cstdint>  // uint16_t
#include <queue>
#include "RayHandler.hpp"
#include "util/util.hpp"

using std::string;
using std::to_string;
using std::vector;
using std::queue;
using boost::posix_time::ptime;
using boost::posix_time::microseconds;
using boost::posix_time::time_duration;
using boost::posix_time::to_simple_string;
using rayptr_t = RayHandler::rayptr_t;

//#define BENCHMARKA

/// Helper function to move 12-bit data from bits 15-4 to bits 11-0 of each 16-bit chunk in the buffer.
static void shift_12bit_data(unsigned char* data, size_t num_samples) {
  uint16_t *ptr(reinterpret_cast<uint16_t*>(data));
  std::transform(ptr, (ptr + num_samples), ptr, [](uint16_t val) {
    // shift each sample
    return val >> 4;
  });

}

void return_handler_fn(SPxHPx100Source *src, void *arg, SPxReturnHeader *hdr, unsigned char *data) {
#ifdef BENCHMARKA
  constexpr int elapsed_n = 10000;
  static int elapsed_count = 0;
  static time_duration elapsed_sum {0, 0, 0, 0};
#endif
  if (eptrs.spx_thread) {
    // Don't try to do anything if this thread errored.
    return;
  }
  try {
    // Get the current time and memory bank right away for downstream processing
    ptime handled_ptime(boost::posix_time::microsec_clock::universal_time());
    unsigned int memory_bank = src->GetBank();
    // Convert arg to ray handler pointer
    RayHandler * handlerPtr = static_cast<RayHandler *>(arg);
    // Do nothing if the consumer thread is down. (Maybe change this to a throw?
    if (eptrs.consumer_thread) {
      SPDLOG_DEBUG("Return handler function quit due to dead consumer thread.");
      return;
    }
    // Handle the ray data with the ray handler. Catch all errors and let the
    handlerPtr->produce_ray(hdr, data, handled_ptime, memory_bank);
  } catch (...) {
    // Share any exceptions in this thread with the rest of the app.
    eptrs.spx_thread = std::current_exception();
    SPDLOG_ERROR("Error in ray handler: " + what(eptrs.spx_thread));
  }

#ifdef BENCHMARKA
  elapsed_sum += ptime(boost::posix_time::microsec_clock::universal_time()) - handled_ptime;
  elapsed_count++;
  if (elapsed_count >= elapsed_n) {
    SPDLOG_TRACE("Elapsed time return_handler_fn, {} rays: {}", elapsed_n,
                 to_simple_string(elapsed_sum));
    elapsed_count = 0;
    elapsed_sum = time_duration(0, 0, 0, 0);
  }
#endif
}

RayHandler::RayHandler(SPxHPx100Source *src, const HPxConfig& config, zmq::context_t & context,
                         string pub_endpoint, bool bind)
    : config(config),
      zcontext(context),
      bind(bind),
      pub_endpoint(pub_endpoint),
      pool(2*src->GetSpokesPerInterrupt(), 2*config.num_samples),
      rays_last_rot(0) {
  running = false;
  STOP = false;
}

void RayHandler::produce_ray(SPxReturnHeader* header, unsigned char *data, const ptime& handled_ptime,
                              unsigned int memory_bank) {
  constexpr unsigned char dummy_data[2] = {0xff, 0xff};
  const unsigned char * data_ptr = nullptr;
  unsigned int data_nbytes = 2;
  rayptr_t rayptr(nullptr);

  // Sanity checks.
  if (!running) {
    if (STOP) {
      // in process of stopping consumer. Just exit the fn.
      return;
    }
    throw RayHandlerError("The consumer must be started before calling the producer.");
  }

  // Determine if this is a new rotation
  if ((int)header->azimuth < pr_state.last_azi) {
    rays_last_rot = pr_state.ray_idx;
    pr_state.ray_idx = 0;
    pr_state.rotation_idx++;
    SPDLOG_DEBUG(string("Handled ") + to_string(pr_state.rotation_idx) + " rotations.");
  }
  pr_state.last_azi = header->azimuth;  // stored to check for new rotation next ray

  // filter and/or shift ray data
  if (azi_in_range(header->azimuth, config.start_azival, config.end_azival)) {
    if (header->packing == SPX_RIB_PACKING_RAW12) {
      shift_12bit_data(data, header->thisLength);
    }
    data_ptr = data;
    data_nbytes = header->thisLength*SPxGetPackingBytesPerSample(header->packing);
  } else {
    data_ptr = dummy_data;
    data_nbytes = 2;
  }

  { // Get Ray from pool and put in queue
    try {
      rayptr = pool.acquire();
    } catch (RayPoolError& err) {
      std::throw_with_nested(RayHandlerError("Error acquiring rayptr from pool."));
    }
    rayptr->set_fields(pr_state.rotation_idx, pr_state.ray_idx, memory_bank, handled_ptime, header->timeInterval,
                        header->azimuth, header->packing, data_ptr, data_nbytes);
    ray_pc_queue.push(std::move(rayptr));
  }
  pr_state.ray_idx++;
}

void RayHandler::ray_consumer() {
  // FUTURE: Add azimuth smoothing
  // loop variables
  queue<rayptr_t> consumer_rays; // buffer for new rays waiting to get a timestamped
  vector<rayptr_t> tstamped_rays(0); // buffer of rays that have been timestamped (vector b/c of tstamping code)
  queue<rayptr_t> rays_to_sum;  // Rays ready to sum
  queue<rayptr_t> rays_to_send;  // Rays ready to send.

  // Set up 0MQ socket
  zmq::socket_t ray_pub(open_pub_socket(zcontext, pub_endpoint, bind));
  // ray_pub.setsockopt(ZMQ_SNDHWM, 1000);  // Default HWM is 1000. Change?

#ifdef BENCHMARKB
#include <chrono>
  using namespace std::chrono;
  high_resolution_clock::time_point consumer_start{high_resolution_clock::now()};
  high_resolution_clock::duration consumer_elapsed{high_resolution_clock::duration::zero()};
#endif

  // Consumer loop
  while (!STOP) {
    /*
     * Move as many rays as available from the producer-consumer queue into the consumer_rays queue.
     * Wait up to 10 us for a ray to become available in ray_pc_queue before moving on -- this is
     * effectively a thread sleep. Note that this means there's always a 10 us sleep even after getting a
     * bunch of rays.
     */
    {
      rayptr_t rayptr;  // needed for ray_queue output var
      while (ray_pc_queue.waitAndPop(rayptr, std::chrono::microseconds(10))) {
        consumer_rays.push(std::move(rayptr));
      }
      // If no rays were available from the producer go back to the top to try again.
      if (consumer_rays.empty()) {
        continue;
      }
    }

    // Work through the queue and move rays into rays_tstamped
    while(!consumer_rays.empty()) {
      if (config.calc_true_timestamps) {
        buffer_and_calc_times(consumer_rays.front(), tstamped_rays);
      } else {
        // No timestamping, just move the ray to the next container
        tstamped_rays.push_back(std::move(consumer_rays.front()));
      }
      consumer_rays.pop();
    }

    // rays_tstamped will be empty if we're waiting for the first full bank. Try for more rays.
    if (tstamped_rays.empty()) continue;

    // Append timstamped rays to queue to be summed (may not be empty).
    for (rayptr_t& ptr: tstamped_rays) {
      rays_to_sum.push(std::move(ptr));
    }
    tstamped_rays.clear();

    // Sum rays
    buffer_and_sum(config.summing_factor, rays_to_sum, rays_to_send);

#ifdef BENCHMARKB
    int nsent_rays = rays_to_send.size();
#endif
    // Send rays
    pack_and_send_rays(ray_pub, rays_to_send);
#ifdef BENCHMARKB
    consumer_elapsed = high_resolution_clock::now() - consumer_start;
    double elapsed_ms = consumer_elapsed.count()/1000000.0;
    SPDLOG_TRACE("SENDING {} rays after {:.1f} ms; {:.3f} ms/ray", nsent_rays, elapsed_ms,
                 elapsed_ms/nsent_rays);
    consumer_start = high_resolution_clock::now();
#endif
  }  // end of consumer loop
  ray_pub.close();
}

/**
 * Keep bank calc_time values from drifting too much and prevent non-increasing timestamps
 * @param bank - vector of rays currently being timestamped
 * @param last_bank_last_time - Last calc_time of the last bank
 */
void adjust_calc_times(vector<rayptr_t>& bank, ptime last_bank_last_time) {
  time_duration time_interval_thresh = microseconds(bank.front()->time_interval)/2;
  ptime first_time = bank.front()->calc_time;
  ptime expected_first_time = last_bank_last_time + microseconds(bank.front()->time_interval);
  // Is the time of the first ray at least a whole time interval older than the last time of the last bank?
  if (first_time >= expected_first_time) {
    // Yes. No action required.
    return;
  }
  // No. Ok, is the difference between expectation and calculation less than half of time_interval?
  time_duration tdiff = expected_first_time - first_time;
  if (tdiff < time_interval_thresh) {
    // Yes. No action required.
    return;
  }
  // No. Ok, adjust all the calc_time values of the bank.
  // Start by making the first calc_time within half a time_interval of the expected time, then linearly add
  // fewer and fewer seconds until the last ray.
  time_duration initial_adjustment = tdiff - time_interval_thresh;
  for (size_t i = 0; i < bank.size(); i++) {
    time_duration adjustment = initial_adjustment*(bank.size()-1 - i)/(bank.size()-1);
    bank[i]->calc_time += adjustment;
  }
  SPDLOG_DEBUG("Adjusted bank times. Initial adjustment: " +
            to_string(initial_adjustment.total_microseconds()) + " us");
}

void RayHandler::buffer_and_calc_times(rayptr_t& consumer_ray, vector<rayptr_t>& tstamped_rays) {
  // Note that this buffer_and_calc_times uses the following class properties to keep state between runs:
  //   rays_to_ts

  // Keep track of bank switches
  int this_bank_i = (int)consumer_ray->memory_bank;  // Saved in case rayptr is moved.
  bool bank_switch = (this_bank_i != bct_state.last_bank_i);
  if (bct_state.num_bank_switches < 2) {  // don't track more than two switches to prevent overflow.
    bct_state.num_bank_switches += (bank_switch) ? 1 : 0;
  }

  // On a bank switch if it's at least the second bank switch then there is a full bank to process. Since
  //    consumer_ray will be part of a new bank, it is not pushed into rays_to_tstamp until after the last
  //    bank is processed.
  if (bank_switch && bct_state.num_bank_switches > 1) {
    // Use the proc_time of the oldest ray in the bank as the 'true' calc_time of the newest ray.
    rays_to_tstamp.back()->calc_time = rays_to_tstamp.front()->proc_time;
    // Calculate the rest by subtracting the microsecond `time_interval`.
    for (int i = rays_to_tstamp.size()-1; i > 0; i--) {
      rays_to_tstamp[i-1]->calc_time = (
        rays_to_tstamp[i]->calc_time - microseconds(rays_to_tstamp[i]->time_interval));
    }
    { // Adjust times as needed to mitigate clock drift.
      if (!bct_state.prev_bank_last_time.is_not_a_date_time()) {
        adjust_calc_times(rays_to_tstamp, bct_state.prev_bank_last_time);
      }
      bct_state.prev_bank_last_time = rays_to_tstamp.back()->calc_time;
    }
    // Push the timestamped rays into the timestamped ray buffer, then clear the rays_to_tstamp buffer.
    for (rayptr_t& ptr: rays_to_tstamp) {
      tstamped_rays.push_back(std::move(ptr));
    }
    rays_to_tstamp.clear();
  }

  // The current ray can now be pushed into rays_to_tstamp so long as it's part of a full bank.
  if (bct_state.num_bank_switches > 0) {
    rays_to_tstamp.push_back(std::move(consumer_ray));
  }
  bct_state.last_bank_i = this_bank_i;  // Save the bank index for comparison with the next ray.
}

void RayHandler::buffer_and_sum(unsigned int summing_factor, queue<rayptr_t>& rays_to_sum,
                                 queue<rayptr_t>& summed_rays) {
  vector<rayptr_t> summing_rays;
  // The summed_rays queue should be empty already, but clear it just in case by swapping with an empty queue
  queue<rayptr_t>().swap(summed_rays);
  if (summing_factor <= 1) {
    // No summing. Move everything to the output buffer
    rays_to_sum.swap(summed_rays);
    return;
  }

  // Make sure packing type is OK (future-proofing)
  if (rays_to_sum.front()->packing != SPX_RIB_PACKING_RAW12) {
    throw RayHandlerError("Summing functionality is not defined for any packing type but 12-bit analog.");
  }

  // Ok, do the summing.
  // summing vars.
  Ray* summing_ray;
  int rot_rollover;
  UINT16 first_azi, last_azi;
  UINT32 azi_sum;
  ptime first_ptime;
  time_duration calc_dt_sum;
  unsigned int interval_sum;
  size_t min_ray_nbytes;

  // Keep creating summed rays until we don't have enough rays to create a summed ray. The remainder will
  // stay in rays_to_sum.
  while (rays_to_sum.size() >= summing_factor) {
    bool azi_filtered = false;
    // Move the rays to sum into their own vector. We'll use the first one (front) as the summed_ray
    for (unsigned int i = 0; i < summing_factor; i++) {
      summing_rays.push_back(std::move(rays_to_sum.front()));
      rays_to_sum.pop();
    }
    // Use the first ray of each group as the summed ray.
    summing_ray = summing_rays.front().get();  // Just get the raw pointer for doing operations

    // Set initial values for summing
    rot_rollover = 0;
    last_azi = first_azi = summing_ray->azimuth;
    azi_sum = (UINT32)first_azi;
    first_ptime = summing_ray->calc_time;
    calc_dt_sum = microseconds(0);
    interval_sum = 0;
    uint16_t* const summed_data_ptr(reinterpret_cast<uint16_t*>(summing_ray->data.ptr()));
    min_ray_nbytes = summing_ray->data.size();
    // Check azi filter
    azi_filtered |= summing_ray->is_filtered();

    Ray* current_ray;
    for(unsigned int i = 1; i < summing_rays.size(); i++) {
      // sum the azimuths, allowing for rollover.
      current_ray = summing_rays[i].get();
      if (current_ray->azimuth < last_azi) {
        rot_rollover += 1;
      }
      azi_sum += (UINT32)current_ray->azimuth + 65536*rot_rollover;

      // Sum the time offsets
      calc_dt_sum += current_ray->calc_time - first_ptime;
      // Sum the intervals
      interval_sum += current_ray->time_interval;

      // Sum data:
      // 1. Get the smaller of the two rays in nbytes.
      min_ray_nbytes = (std::min(current_ray->data.size(), min_ray_nbytes)/2)*2; // min even #bytes
      // 2. Get the uint16_t pointer to this ray.
      uint16_t* const this_data_ptr(reinterpret_cast<uint16_t*>(current_ray->data.ptr()));
      // 2.5 check azi filter and don't bother summing if filtered.
      azi_filtered |= current_ray->is_filtered();
      if (!azi_filtered) {
        // 3. Add the values from this ray's data array to the summed ray's data array.
        for (int sample_i = 0; sample_i < (int)min_ray_nbytes/2; sample_i++) {
          summed_data_ptr[sample_i] += this_data_ptr[sample_i];
        }
      }
      // Get ready for next ray
      last_azi = current_ray->azimuth;
    }

    // Set values of summed ray.
    // Set dummy data to signify filtered if azi_filtered
    if (azi_filtered) {
      summed_data_ptr[0] = 0xffff;
      min_ray_nbytes = 2;
    }

    // Average azimuth and rollover rotation_idx if necessary
    summing_ray->azimuth = (azi_sum/summing_factor) % 65536;
    if (summing_ray->azimuth < first_azi) {
      summing_ray->rotation_idx += 1;
    }
    if ((int)summing_ray->rotation_idx > bs_state.last_rot_idx) {
      bs_state.summed_ray_idx = 0;
    }
    bs_state.last_rot_idx = summing_ray->rotation_idx;
    summing_ray->ray_idx = bs_state.summed_ray_idx;

    // Average time difference and add to the time of the first ray.
    summing_ray->calc_time = summing_ray->calc_time + (calc_dt_sum/summing_factor);

    // Save the summed time interval
    if (interval_sum > 65535) {
      // There was more than 65535 us between summed rays.
      // FUTURE: Alert clients? This is unlikely.
      interval_sum = 65535;
      SPDLOG_WARN("Maximum time interval exceeded. Setting to 65535.");
    }
    summing_ray->time_interval = (UINT16)interval_sum;

    // resize the summed ray to the size of the smallest ray (data was already summed above)
    summing_ray->data.shrink_to(min_ray_nbytes);

    // Move the actual rayptr_t pointer of the summing ray to the output array.
    summed_rays.push(std::move(summing_rays.front()));
    bs_state.summed_ray_idx += 1;

    // Clear the summing vector, which effectively calls the destructor on the remaining rays.
    summing_rays.clear();
  }
  return;
}

void RayHandler::pack_and_send_rays(zmq::socket_t& pub_sock, std::queue<rayptr_t>& rays_to_send) const {
  // Each RAY message is a multipart ZeroMQ message composed of an "envelope" string "RAY" and the packed
  // Ray message. We're able to reuse the body_msg object for each ray (hopefully avoiding much  memory
  // reallocation).
  static zmq::message_t body_msg;  // Re-use this which should be able to limit memory reallocation.

  while (rays_to_send.size() > 0) {
    if (config.send_filtered || !rays_to_send.front()->is_filtered()) {
      rays_to_send.front()->pack();
      body_msg.rebuild(rays_to_send.front()->sbuf.data(), rays_to_send.front()->sbuf.size());
      zmq_send_envelope(pub_sock, "RAY", body_msg, true);
    }
    rays_to_send.pop();
  }
}
