/**
 * @file RayHandler.hpp
 * @brief Declaration of RayHandler2 class and helper functions.
 *
 * @date Nov 1, 2019
 * @author Randall Pittman <Randall.Pittman@oregonstate.edu>
 * @copyright MIT (see LICENSE.txt)
 */

#ifndef SRC_RAYHANDLER2_HPP_
#define SRC_RAYHANDLER2_HPP_

#include <atomic>
#include <memory>
#include <queue>
#include <string>
#include <thread>
#include <vector>
#include <zmq.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <SPxLibData/SPxHPx100Source.h>

#include "util/util.hpp"
#include "HPxConfig.hpp"
#include "RayPool.hpp"
#include "RayPCQ.hpp"

/**
 * Data function to install in the HPx card API to handle earch radar return.
 *
 * @param src - HPx card source
 * @param arg - arbitrary argument. Should be a RayHandler2 instance.
 * @param hdr - HPx radar return header struct
 * @param data - Data array (length defined by hdr->thisLength)
 */
void return_handler_fn(SPxHPx100Source *src, void *arg, SPxReturnHeader *hdr, unsigned char *data);

/**
 * Producer/consumer class to handle incoming rays from the HPx card.
 *
 * Each time a ray comes in from the HPx card, it should be passed to `produce_ray` which will put in in a
 * Ray object from the Ray object pool and put it in the queue for processing. `ray_consumer` then executes
 * the downstream processing on each ray that comes in on the queue, including azimuth filtering,
 * timestamp determination, and ray summing.
 */
class RayHandler {
 public:
  // Type aliases
  /// rayptr_t is a unique pointer with a custom deleter.
  using rayptr_t = RayPool::ptr_type;

  /**
   * Constructor sets up the pool of pre-allocated rays and the queue for passing from producer to consumer.
   *
   * @param src - HPx card data source
   * @param config - HPxConfig object
   * @param context - Common context for zmq sockets.
   * @param pub_endpoint - Endpoint to send Ray messages. When using a proxy, this should be the proxy
   *   endpoint.
   * @param bind - If true, the socket binds to the address. If false, it connects. If going through a
   *   proxy, bind should be false.
   */
  RayHandler(SPxHPx100Source *src, const HPxConfig& config, zmq::context_t & context,
              std::string pub_endpoint, bool bind = false);

  /// Deleted copy constructor.
  RayHandler(const RayHandler &) = delete;

  /// Destructor. Just calls stop().
  ~RayHandler() {
    stop();
  }

  /**
   * Handle a radar return from the HPx card.
   *
   * Called by a simple function installed to the HPx card API. This function gets a Ray from the Ray pool,
   * copies in the data from the HPx card, and puts it in the queue for processing.
   * Note: `ray_consumer` should already be running or the queue may overflow.
   * @param header - HPx radar return header struct
   * @param data - Data array (length defined by header->thisLength)
   * @param handled_ptime - System timestamp from when the ray was handled.
   * @param memory_bank - HPx card memory bank being read from at the time the ray was handled.
   *
   * Notes:
   *  - The function does modify `data` if the return is 12-bit packed because it shifts the bits down to the
   *    LSBs.
   */
  void produce_ray(SPxReturnHeader* header, unsigned char *data,
                   const boost::posix_time::ptime& handled_ptime, unsigned int memory_bank);

  /// Start the consumer thread.
  void run() {
    consumer_thread = std::thread(&RayHandler::consumer_thread_wrapper, this);
    running = true;
  }

  /// Stop the consumer thread.
  void stop() {
    STOP = true;
    if (consumer_thread.joinable()) {
      consumer_thread.join();
      SPDLOG_TRACE("Ray Consumer thread joined.");
    }
    running = false;
    STOP = false;  // reset in case we want to restart the consumer.
  }

  /// Value of private member `running`
  bool is_running() {
    return running;
  }

 private:
  const HPxConfig& config;
  zmq::context_t& zcontext;
  bool bind;
  std::string pub_endpoint;
  RayPool pool;
  /// ray_queue is for passing Ray pointers from the producer to the consumer.
  RayPCQ ray_pc_queue;
  /// Indicates whether the consumer is running
  std::atomic_bool running;
  /// Tells the consumer to stop.
  std::atomic_bool STOP;
  std::thread consumer_thread;

 public:
  /// Number of rays in last rotation
  std::atomic_uint rays_last_rot;

 private:
  // produce_ray state variables
  struct pr_state {
    int last_azi = -1;
    unsigned int ray_idx = 0;
    unsigned int rotation_idx = 0;
  } pr_state;

  // buffer_and_calc_times state variables
  std::vector<rayptr_t> rays_to_tstamp;
  struct bct_state {
    int last_bank_i = -1;
    int num_bank_switches = 0;
    boost::posix_time::ptime prev_bank_last_time = boost::posix_time::not_a_date_time;
  } bct_state;

  // buffer_and_sum state variables
  struct bs_state {
    int last_rot_idx = -1;
    unsigned int summed_ray_idx = 0;
  } bs_state;

  /// `ray_consumer` is run in `consumer_thread` and does multi-ray processing, packing, and sending.
  void ray_consumer();

  /// Wrapper for ray_consumer to catch all exceptions and put them in the consumer thread exception pointer.
  void consumer_thread_wrapper() {
    try {
      ray_consumer();
    } catch (...) {
      eptrs.consumer_thread = std::current_exception();
      SPDLOG_ERROR("Error in ray handler consumer: " + what(eptrs.consumer_thread));
    }
  }
  /**
   * Buffer up rays so they can be timestamped, then release the buffer when they are timestamped.
   *
   * @param[in,out] consumer_ray - New rayptr from the consumer to possibly put in the buffer. If at least
   *   one memory-bank-switch has occurred, the pointer is moved into the buffer and thus is null after this
   *   function exits. Otherwise it is left as-is.
   * @param[out] tstamped_rays - Output buffer of (pointers to) timetstamped ray. This returns empty until a
   *   memory bank switch.
   */
  void buffer_and_calc_times(rayptr_t& consumer_ray, std::vector<rayptr_t>& tstamped_rays);

  /**
   * Process a queue of rays to be summed and output summed rays.
   *
   * @param summing_factor - Number of rays per summed ray
   * @param[in,out] rays_to_sum -  queue of rays to be summed. If there are not a `summing_factor` multiple
   *   of rays to sum, the remainder stay in the queue.
   * @param[out] summed_rays - Queue of summed rays.
   */
  void buffer_and_sum(unsigned int summing_factor, std::queue<rayptr_t>& rays_to_sum,
                      std::queue<rayptr_t>& summed_rays);

  /**
   * Pack and send each ray from a queue of ray pointers.
   * @param pub_sock - 0MQ socket created in `ray_consumer()`
   * @param rays_to_send - queue of `rayptr_t` rays ready to send.
   */
  void pack_and_send_rays(zmq::socket_t& pub_sock, std::queue<rayptr_t>& rays_to_send) const;
};

#endif /* SRC_RAYHANDLER2_HPP_ */
