/**
 * @file RayPool.hpp
 * @brief Definition and implementation of the RayPool class.
 *
 * @date Oct 28, 2019
 * @author Randall Pittman <Randall.Pittman@oregonstate.edu>
 * @copyright MIT (see LICENSE.txt)
 */

#ifndef RAYPOOL_HPP_
#define RAYPOOL_HPP_

#include <mutex>
#include <memory>
#include <stack>

#include "Ray.hpp"
#include "SpinLock.hpp"
#include "util/errors.hpp"
#include "util/logging.hpp"

// FUTURE: Consider replacing SpinLock with mutex

/**
 * RayPool is an object pool of Rays. (Or rather unique_ptrs to Rays).
 *
 * When a Ray pointer's deleter is called (goes out of scope in the code using the ray) it is automatically
 * restored to the pool.
 *
 * Based on https://stackoverflow.com/a/27837534/2196270.

 * Usage:
 * @code
 * // Initialize a pool of ten Rays, each with a maximum size of 4096 bytes
 * RayPool pool(10, 4096);
 * cout << pool.size() << " ";  // Prints 10
 * using rayptr_t = RayPool::ptr_type;  // Get ray pool pointer type (std::unique_ptr<Ray, ExternalDeleter>)
 * // Option one: Get a rayptr in a scope, ray is automatically restored to the pool at the end of the scope.
 * {
 *   rayptr_t rayptr = pool.acquire();
 *   cout << pool.size() << " ";  // Prints 9
 *   // Do stuff with `rayptr` (see docs for the Ray class)
 * }  // rayptr is destroyed, and the deleter automatically returns the ray to the pool.
 * cout << pool.size() << " ";  // Prints 10
 *
 * // Option two: Get a rayptr in a scope, but then move it to some other structure that outlives that scope.
 * {
 *   std::queue<rayptr_t> ray_queue;
 *   cout << ray_queue.size() << "\n";  // Prints 0
 *   for (int i = 0; i < 10; i++) {
 *     rayptr_t rayptr = pool.acquire();
 *     cout << pool.size() << " ";  // Prints 9 8 7 ... 0
 *     // maybe do stuff with rayptr.
 *     // Move-push it to the queue. (std::move does move of the pointer, rather than a copy).
 *     ray_queue.push(std::move(rayptr));
 *     // Now rayptr just has a nullptr, so no deleter function is called when it is destroyed.
 *   }
 *   // rays are not returned to the pool yet because now they are in ray_queue.
 *   cout << "\n";
 *   cout << ray_queue.size() << "\n";  // Prints "10".
 *   cout << pool.size() << "\n";  // Prints "0".
 * }
 * // Now all ten rays are returned to the pool as ray_queue (which holds the ray pointers) gets destroyed.
 * cout << ray_queue.size() << "\n";  // Prints "0".
 * cout << pool.size() << "\n";  // Prints "10".
 * @endcode
 *
 */
class RayPool {
 private:
  /// A pointer to `this`, which can be used by the external deleter to push the
  std::shared_ptr<RayPool*> this_ptr_;
  // The internal stack of Rays.
  std::stack<std::unique_ptr<Ray>> pool_;
  /// Simple spinlock for lock ops since lock times will be very short.
  SpinLock mtx;
  /**
   * Deleter functor for the Ray pointers. When a ray pointer is deleted, it is restored to the pool.
   *
   * Includes check to ensure that the pool is still around. If not, the Ray is deleted.
   */
  struct External_Deleter {
      explicit External_Deleter(std::weak_ptr<RayPool* > pool)
          : pool_(pool) {}
      explicit External_Deleter(){}

      void operator()(Ray* ptr) {
        if (auto pool_ptr = pool_.lock()) {
          try {
            (*pool_ptr.get())->restore(std::unique_ptr<Ray>{ptr});
            return;
          } catch(...) {}
        }
        std::default_delete<Ray>{}(ptr);
      }
     private:
      std::weak_ptr<RayPool* > pool_;
    };
 public:
  /// External code should use ptr_type for ray pointers acquired from the pool. (See Usage.)
  using ptr_type = std::unique_ptr<Ray, External_Deleter >;
  /**
   * Set up the pool, allocating as many Rays as we want.
   * @param init_nrays          Total number of Rays to allocate in the pool.
   * @param max_raydata_nbytes  Max size of data in each Ray.
   * @param grow_factor         If > 0, the pool grows by this many rays as needed. Default 32.
   * @param max_pool_nbytes     Max number of bytes bool can use. Default 100 MiB.
   */
  RayPool(size_t init_nrays, size_t max_raydata_nbytes, size_t grow_factor = 32,
          size_t max_pool_nbytes = 100*1024*1024)
      : this_ptr_(new RayPool*(this)), total_nrays(init_nrays), max_raydata_nbytes(max_raydata_nbytes),
        grow_factor(grow_factor), max_pool_nbytes(max_pool_nbytes) {
    /// wait for allocation to complete before allowing the pool to be used.
    std::lock_guard<SpinLock> lock(mtx);
    for (size_t i = 0; i < init_nrays; i++) {
      pool_.push(std::move(std::unique_ptr<Ray>(new Ray(max_raydata_nbytes))));
    }
  }
  /// Return a Ray pointer to the pool. This should not be called explicitly, but rather is only called by
  /// the deleter.
  void restore(std::unique_ptr<Ray> rayPtr) {
    std::lock_guard<SpinLock> lock(mtx);
    rayPtr->reset();
    pool_.push(std::move(rayPtr));
  }
  /// Get a Ray pointer from the pool.
  ptr_type acquire(){
    std::lock_guard<SpinLock> lock(mtx);
    while (pool_.empty()) {
      if ((total_nrays + grow_factor)*max_raydata_nbytes < max_pool_nbytes) {
        for (size_t i = 0; i < grow_factor; i++) {
          pool_.push(std::move(std::unique_ptr<Ray>(new Ray(max_raydata_nbytes))));
        }
        total_nrays += grow_factor;
        SPDLOG_DEBUG("RayPool grown to " + std::to_string(total_nrays));
      } else {
        throw RayPoolError("Cannot acquire a Ray; the pool cannot grow to offer more rays."
                           " Max num rays in pool: " +
                           std::to_string(int(max_pool_nbytes/max_raydata_nbytes)));
      }
    }
    ptr_type rp(pool_.top().release(), External_Deleter{std::weak_ptr<RayPool*>{this_ptr_}});
    pool_.pop();
    return rp; // Implicit move/copy-elision for local variable return
  }
  /// Get the pool size.
  size_t size() const {
    return pool_.size();
  }
 private:
  size_t total_nrays;
  size_t max_raydata_nbytes;
  size_t grow_factor;
  size_t max_pool_nbytes;
};

#endif /* RAYPOOL_HPP_ */
