/**
 * @file logging2.hpp
 * @brief Logging setup
 *
 * @date Aug 4, 2020
 * @author Randall Pittman <Randall.Pittman@oregonstate.edu>
 * @copyright MIT (see LICENSE.txt)
 */

#ifndef SRC_UTIL_LOGGING2_HPP_
#define SRC_UTIL_LOGGING2_HPP_

#include <algorithm>
#include <memory>
#include <sstream>
#include <stdexcept>
#include <vector>

#define SPDLOG_ACTIVE_LEVEL 0 // 0 trace, 1 debug, 2 info... see spdlog/common.h
#include <spdlog/spdlog.h>
#include <spdlog/sinks/stdout_color_sinks.h>
#include <spdlog/sinks/systemd_sink.h>

#include "PipePair.hpp"

namespace loglevel = spdlog::level;

// Macro to log at some variable level to the default logger

#define SPDLOG_LOG(level, ...) SPDLOG_LOGGER_CALL(spdlog::default_logger_raw(), level, __VA_ARGS__)

/**
 * Set up a logger for spdlog and make it the default logger.
 *
 * The created logger has both stderr and journald sinks, each of which may be set to have a
 * different verbosity, as specified by `console_level` and `journald_level`, respectively.
 *
 * These may be set to any valid spdlog level (any others will result in a runtime error).:
 *
 * SPDLOG_LEVEL_TRACE 0
 * SPDLOG_LEVEL_DEBUG 1
 * SPDLOG_LEVEL_INFO 2
 * SPDLOG_LEVEL_WARN 3
 * SPDLOG_LEVEL_ERROR 4
 * SPDLOG_LEVEL_CRITICAL 5
 * SPDLOG_LEVEL_OFF 6
 *
 * @param console_level  Minimum spdlog message level to report on the console.
 * @param journald_level Minimum spdlog message level to report to journald.
 */
inline void log_init(
    loglevel::level_enum console_level, loglevel::level_enum journald_level) {
  std::vector<spdlog::sink_ptr> sinks;
  auto stderr_sink = std::make_shared<spdlog::sinks::ansicolor_stderr_sink_mt>();
  stderr_sink->set_level(console_level);
  auto journald_sink = std::make_shared<spdlog::sinks::systemd_sink_mt>();
  journald_sink->set_level(journald_level);
  sinks.push_back(stderr_sink);
  sinks.push_back(journald_sink);
  auto logger = std::make_shared<spdlog::logger>("hpx-radar-server", begin(sinks), end(sinks));
  logger->set_level(std::min(console_level, journald_level));
  //register it if you need to access it globally
  spdlog::register_logger(logger);
  spdlog::set_default_logger(logger);
}


/**
 * Emit any lines from the SPx log as log messages
 *
 * @param pp PipePair SPx log is writing to.
 * @param level Level for log message.
 */
inline void capture_spx_log(PipePair& pp, loglevel::level_enum level) {
  std::stringstream spx_log(pp.read());
  std::string spx_log_text;
  if (spx_log.str() != "") {
    while (std::getline(spx_log, spx_log_text, '\n')) {
      SPDLOG_LOG(level, "SPx Log: {}", spx_log_text);
//      spdlog::default_logger_raw()->log(level, "SPx Log: {}", spx_log_text);
    }
  }
}


#endif /* SRC_UTIL_LOGGING2_HPP_ */
