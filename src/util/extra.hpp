/**
 * @file extra.hpp
 * @brief Various helper functions.
 *
 * @date Oct 31, 2019
 * @author Randall Pittman <Randall.Pittman@oregonstate.edu>
 * @copyright MIT (see LICENSE.txt)
 */
/*
 * extra.hpp
 *
 *  Created on: Oct 31, 2019
 *      Author: pittmara
 */

#ifndef SRC_UTIL_EXTRA_HPP_
#define SRC_UTIL_EXTRA_HPP_

#include <cstdio>
#include <memory>
#include <iomanip>
#include <sstream>
#include <string>
#include <dirent.h>

#include "errors.hpp"


/// Check if 16-bit azimuth is in the range [from, to). Allows for zero-crossing.
inline bool azi_in_range(int angle, int from, int to) {
  while (to <= from)
    to += 65536;
  while (angle < from)
    angle += 65536;
  return (angle >= from && angle < to);
}

/**
 * Check if a file exists and is readable.
 *
 * @param fpath Path to the file
 * @return File exists and may be read.
 */
inline bool fexist(std::string fpath) {
  FILE* fp;
  bool read_ok = false;
  if ((fp = fopen(fpath.c_str(), "r")) != NULL) {
    read_ok = true;
    fclose(fp);
  }
  return read_ok;
}

/// Determine if a directory exists and may be accessed.
inline bool dexist(const std::string& dirpath) {
  DIR* dir = opendir(dirpath.c_str());
  if (dir) {
      /* Directory exists. */
      closedir(dir);
      return true;
  } else if (ENOENT == errno) {
      return false;
  } else {
      throw CError("opendir failed");
  }
}

/// Get an enviroment variable as a string (empty string if missing)
inline std::string get_env_str(const char * key, std::string default_="") {
  char * ret_val(std::getenv(key));
  return ret_val != NULL ? std::string(ret_val) : default_;
}

/// Get an enviroment variable as a int. Returns `default_` if missing or not convertible.
inline int get_env_int(const char * key, int default_=0) {
  int env_int;
  std::string envstr = get_env_str(key);
  try {
    env_int = std::stoi(envstr);
  } catch (std::logic_error& err) {
    // Can't convert to str.
    env_int = default_;
  }
  return env_int;
}

/// Get the user's HOME directory path
inline std::string get_home() {
  // NOTE: Needs win32 impl.
  return get_env_str("HOME");
}

/// Get a hex representation of an integer `value`, with at least `min_width` characters
inline std::string hexstr(int value, int min_width = 1) {
  std::ostringstream s;
  s << std::hex << std::setw(min_width) << std::setfill('0') << value;
  return s.str();
}

#endif /* SRC_UTIL_EXTRA_HPP_ */
