/**
 * @file util/PipePair.hpp
 * @brief PipePair class
 *
 * @date Aug 11, 2020
 * @author Randall Pittman <Randall.Pittman@oregonstate.edu>
 * @copyright MIT (see LICENSE.txt)
 */

#ifndef SRC_UTIL_PIPEPAIR_HPP_
#define SRC_UTIL_PIPEPAIR_HPP_

#include <cstdio>
#include <cstring>
#include <unistd.h>
#include <linux/limits.h>
#include <fcntl.h>
#include <stdexcept>
#include <string>

#include "errors.hpp"


/**
 * Class to manage an anonymous POSIX pipe with C FILE pointer access.
 *
 * The SPx SetLogFile function accepts a C FILE pointer for the log file, but we'd rather capture
 * the SPx library log text in our own logging setup. This class creates and opens a non-blocking POSIX pipe
 * and gets FILE pointers to each end (read and write) of the pipe.
 * 
 * The FILE pointers may be directly accessed with `fp_read` and `fp_write`, but the `read()` method is best
 * for returning all the contents of the pipe. 
 *
 */
class PipePair {
 private:
  int fd_read, fd_write;
 public:
  FILE* fp_read;
  FILE* fp_write;
  /**
   * Create the pipe and open FILE pointers to each end of the pipe.
   */
  PipePair() {
    int pipe_fds[2];
    if (pipe2(pipe_fds, O_NONBLOCK) != 0) {
      throw CError("Error creating pipe.");
    }
    fd_read = pipe_fds[0];
    fd_write = pipe_fds[1];
    if ((fp_read = fdopen(fd_read, "r")) == nullptr) {
      throw CError("Error opening the pipe for reading");
    }
    if ((fp_write = fdopen(fd_write, "w")) == nullptr) {
      throw CError("Error opening the pipe for writing");
    }
  }
  ~PipePair() {
    if (fclose(fp_write) != 0) {
      perror("Unable to close the pipe writer");
    }
    if (fclose(fp_read) != 0) {
      perror("Unable to close the pipe reader");
    }
  }
  /**
   * Get all the contents of the pipe.
   * @return Pipe contents.
   */
  std::string read() {
    static std::string out;
    static bool first_run = true;
    if (first_run) {
      out.reserve(PIPE_BUF*100);  // We're not short on space. Assume up to 100 4k buffers.
      first_run = false;
    }
    out = "";
    char c = 0;

    fflush(fp_write);
    while (true) {
      c = fgetc(fp_read);
      if (c == EOF) break;
      out.append(&c, 1);
    }
    return out;
  }
};

#endif /* SRC_UTIL_PIPEPAIR_HPP_ */
