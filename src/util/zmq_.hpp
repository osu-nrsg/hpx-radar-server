/**
 * @file zmq_.hpp
 * @brief Helper functions for ZeroMQ operations.
 *
 * @date Nov 11, 2019
 * @author Randall Pittman <Randall.Pittman@oregonstate.edu>
 * @copyright MIT (see LICENSE.txt)
 */
/*
 * zmq.hpp
 *
 *  Created on: Nov 11, 2019
 *      Author: pittmara
 */

#ifndef SRC_UTIL_ZMQ__HPP_
#define SRC_UTIL_ZMQ__HPP_

#include <string>
#include <sys/stat.h>
#include <unistd.h>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <msgpack.hpp>
#include <zmq.hpp>
#include "util/util.hpp"


/// Get the path of the ipc:// socket if it is one, otherwise return "".
inline std::string ipc_address(std::string endpoint) {
  auto found = endpoint.find("://");
  if (found <= 0 || endpoint.length() <= found+3) {
    return "";
  }
  std::string protocol = endpoint.substr(0, found);
  if (protocol != "ipc") {
    return "";
  }
  return endpoint.substr(found+3, std::string::npos);
}

/// Make sure if the socket is Unix that is has group rw permissions
inline void update_socket_perms(const std::string& endpoint) {
  std::string address = ipc_address(endpoint);
  if (address != "") {
    chmod(address.c_str(), 0770);
    SPDLOG_TRACE("Socket {} permissions set to 770", address);
  }
}

inline void remove_socket(const std::string& endpoint) {
  std::string address = ipc_address(endpoint);
  if (address != "") {
    unlink(address.c_str());
    SPDLOG_TRACE("Unlinked socket {}", address);
  }
}

/// Make ZeroMQ message from a std::string.
inline zmq::message_t zmqstrmsg(const std::string& str) {
  return zmq::message_t(str.data(), str.size());
}

/**
 * Send a ZeroMQ multipart message to `socket` with parts `address`, `body`, and optionally a timestamp
 * string.
 *
 * See http://zguide.zeromq.org/page:all#Pub-Sub-Message-Envelopes.
 * @param socket - 0MQ socket to send muessage
 * @param address - Pub/sub "envelope" (subscriber can filter on this string)
 * @param body - 0MQ message contents
 * @param timestamp - If 'true', append the current time as a msgpack timestamp. Default false.
 */
inline void zmq_send_envelope(zmq::socket_t& socket, std::string address, zmq::message_t& body,
                              bool timestamp = false) {
  using namespace boost::posix_time;
  msgpack::sbuffer sbuf(15);  // Max size of a timestamp message is 15 bytes.
  if (!socket) {
    throw HPxError("Socket is gone.");
  }
  socket.send(zmqstrmsg(address), zmq::send_flags::sndmore);
  if (timestamp) {
    msgpack::pack(sbuf, ptime(microsec_clock::universal_time()));
    socket.send(body, zmq::send_flags::sndmore);
    socket.send(zmq::message_t(sbuf.data(), sbuf.size()), zmq::send_flags::none);
  } else {
    socket.send(body, zmq::send_flags::none);
  }
}

/**
 * Open and bind/connect to a ZeroMQ PUB socket.
 *
 * @param zcontext - 0MQ context
 * @param endpoint - 0MQ endpoint; e.g. "ipc://...", "tcp://...", or "inproc://..."
 * @param bind - If true, bind to the endpoint, if false, connect (e.g. if connecting to an XSUB)
 * @param linger - ms for socket to continue sending remaning messages after socket or context close. 0
 *        discards all remaining messages, -1 blocks until all are sent.
 * @return zmq::socket_t type that has been created and bound/connected.
 */
inline zmq::socket_t open_pub_socket(zmq::context_t& zcontext, const std::string& endpoint, bool bind = true,
                                     int linger = 0) {
  zmq::socket_t pub_sock(zcontext, zmq::socket_type::pub);
  if (bind) {
    pub_sock.bind(endpoint);
  } else {
    pub_sock.connect(endpoint);
  }
  pub_sock.set(zmq::sockopt::linger, linger);
  return pub_sock;
}

#endif /* SRC_UTIL_ZMQ__HPP_ */
