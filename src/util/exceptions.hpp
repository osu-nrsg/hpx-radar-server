/**
 * @file util/exceptions.hpp
 * @brief Thread exceptions pointers and exception printing helper functions.
 *
 * @date Nov 15, 2019
 * @author Randall Pittman <Randall.Pittman@oregonstate.edu>
 * @copyright MIT (see LICENSE.txt)
 */

#ifndef SRC_UTIL_EXCEPTIONS_HPP_
#define SRC_UTIL_EXCEPTIONS_HPP_

#include <exception>
#include <string>
#include <typeinfo>

// exception pointers to share thread exception state.
struct exception_ptrs {
  std::exception_ptr spx_thread;
  std::exception_ptr spxerr_thread;
  std::exception_ptr consumer_thread;
  bool any() {
    return bool(spx_thread || spxerr_thread || consumer_thread);
  }
};

/// exception pointers to share thread exception state.
extern exception_ptrs eptrs;

/**
 * Get a string representation of any std::exception, std::string or char* exception from exception_ptr.
 * @param eptr - std::exception_ptr
 * @return String including, if possible, error type and error what() string.
 */
inline std::string what(const std::exception_ptr &eptr = std::current_exception(), bool show_typeid = true);

/// Helper for `what()` to print nested exceptions
template <typename T>
inline std::string nested_what(const T &e) {
  try { std::rethrow_if_nested(e); }
  catch (...) { return " (" + what(std::current_exception()) + ")"; }
  return {};
}

/// Get string representation of exceptions, including nested exceptions.
inline std::string what(const std::exception_ptr &eptr, bool show_typeid) {
  if (!eptr) { throw std::bad_exception(); }

  try { std::rethrow_exception(eptr); }
  catch (const std::exception &e) {
    return (show_typeid ? ("[" + std::string(typeid(e).name()) + "] ") : ("")) + e.what() + nested_what(e);
  }
  catch (const std::string    &e) { return e          ; }
  catch (const char           *e) { return e          ; }
  catch (...)                     { return "Unstring-able exception type."; }
  return "";  // Never get here, but prevent IDE warning.
}

#endif /* SRC_UTIL_EXCEPTIONS_HPP_ */
