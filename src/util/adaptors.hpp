/**
 * @file adaptors.hpp
 * @brief msgpack-c adapators.
 *
 * RawArray class and MessagePack adaptors for RawArray and boost::posix_time::ptime.
 *
 * @date Oct 26, 2018
 * @author Randall Pittman <Randall.Pittman@oregonstate.edu>
 * @copyright MIT (see LICENSE.txt)
 */

#ifndef SRC_ADAPTORS_HPP_
#define SRC_ADAPTORS_HPP_

#include <algorithm>
#include <cstring>
#include <ctime>
#include <limits>
#include <string>
#include <msgpack.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

/**
 * Dyanmic data unsigned char array designed to be allocated once and reused many times.
 *
 */
class RawArray {
 private:
  /// Allocated size of the buffer
  size_t _max_size;
  /// Virtual size of the buffer, <= _max_size.
  size_t _size;
  /// Pointer to the buffer
  unsigned char* _ptr;
 public:
  /// Default constructor; Allocate nothing.
  RawArray() : _max_size(0), _size(0), _ptr(nullptr) {}
  /// Allocate-only constructor. Allocate buffer for data.
  RawArray(size_t max_size)
      : _max_size(max_size),
        _size(0),
        _ptr(new unsigned char[max_size]) {}
  /// Allocate-and-fill constructor -- Allocate buffer and copy data into the buffer.
  RawArray(size_t max_size, const unsigned char * data, size_t data_sz)
      : _max_size(max_size),
        _size(0),
        _ptr(new unsigned char[max_size]) {
    set_data(data, data_sz);
  }
  /// Destructor -- release the allocated memory.
  ~RawArray() {
    delete [] _ptr;
  }
  /// Delete the allocated memory and reallocate new memory. Old data is lost.
  void realloc(size_t max_size) {
    delete [] _ptr;
    this->_max_size = max_size;
    this->_ptr = new unsigned char[max_size];
  }
  /// Copy data into the buffer. data may be smaller than the buffer.
  void set_data(const unsigned char* const data, size_t sz) {
    if (sz > _max_size) {
      throw std::length_error("RawArray data size is limited to max_size.");
    }
    std::memcpy(_ptr, data, sz);
    _size = sz;
  }
  /// Shrink virtual size of the array.
  void shrink_to(size_t new_size) {
    if (new_size > _size) {
      throw std::range_error("Cannot shrink to a size larger than current size.");
    }
    _size = new_size;
  }
  /// Pointer to the data buffer (data is mutable, pointer is immutable).
  unsigned char * ptr() const {
    return _ptr;
  }
  /// Current virtual size of the data buffer (<= allocated buffer space).
  const size_t& size() const {
    return _size;
  }
  /// Get the value of any element in the array by index up to the virtual size.
  unsigned char& operator[](size_t iloc) const {
    if (iloc >= _size) {
      throw std::out_of_range("Index is too large.");
    }
    return _ptr[iloc];
  }
  /// RawArray equality test
  friend bool operator==(RawArray const& lhs, RawArray const& rhs) {
    return lhs.size() == rhs.size() && (std::memcmp(lhs.ptr(), rhs.ptr(), lhs.size()) == 0);
  }
};

#ifndef _MSGPACK_NAMESPACE_STR
//#define _MSGPACK_NAMESPACE_STR MSGPACK_API_VERSION_NAMESPACE( MSGPACK_DEFAULT_API_NS )
#define _MSGPACK_NAMESPACE_STR inline namespace v3
#endif

namespace msgpack {
_MSGPACK_NAMESPACE_STR {
namespace adaptor {

// --RawArray msgpack functions--
/// Convert msgpack object to RawArray.
template <>  // template specialization syntax
struct convert<RawArray> {
  msgpack::object const& operator()(msgpack::object const& o , RawArray& v) const {
    if (o.type != msgpack::type::BIN) throw msgpack::type_error();
    v.realloc(o.via.bin.size);
    v.set_data(reinterpret_cast<const unsigned char *>(o.via.bin.ptr), o.via.bin.size);
    return o;
  }
};

/// Pack RawArray into a msgpack stream packer.
template <>
struct pack<RawArray> {
  template <typename Stream>
  msgpack::packer<Stream>& operator()(msgpack::packer<Stream>& o, RawArray const& v) const {
    o.pack_bin(v.size());
    o.pack_bin_body(reinterpret_cast<const char *>(v.ptr()), v.size());
    return o;
  }
};
// --END RawArray msgpack functions--

// --ptime msgpack functions--
#ifdef HPX_STR_TIMESTAMP
/// Convert msgpack str object to boost::posix_time::ptime.
template <>
struct convert<boost::posix_time::ptime> {
  msgpack::object const& operator()(msgpack::object const& o , boost::posix_time::ptime& v) const {
    if (o.type != msgpack::type::STR) throw msgpack::type_error();
    std::string tstring;
    o.convert(tstring);
    if (tstring.back() == 'Z') {
      tstring.pop_back();
    }
    v = boost::date_time::parse_delimited_time<boost::posix_time::ptime>(tstring, 'T');
    return o;
  }
};

/// Pack boost::posix_time::ptime as an ISO8601 string into a msgpack stream packer.
template <>
struct pack<boost::posix_time::ptime> {
  template <typename Stream>
  msgpack::packer<Stream>& operator()(msgpack::packer<Stream>& o, boost::posix_time::ptime const& v) const {
    if (!v.is_not_a_date_time()) {
      o.pack(boost::posix_time::to_iso_extended_string(v) + "Z");
    } else {
      o.pack("");
    }
    return o;
  }
};

#else /* (not) HPX_STR_TIMESTAMP -- use msgpack timestamp */

/**
 * Convert msgpack Timestamp object to boost::posix_time::ptime.
 *
 * Caution! This implementation is not Y2038 safe!
 * FUTURE: Switch to using std::chrono::time_point over ptime, or something else.
 */
template <>
struct convert<boost::posix_time::ptime> {
  msgpack::object const& operator()(msgpack::object const& o, boost::posix_time::ptime& v) const {
    if(o.type != msgpack::type::EXT) { throw msgpack::type_error(); }
    if(o.via.ext.type() != -1) { throw msgpack::type_error(); }
    boost::posix_time::ptime pt;
    switch(o.via.ext.size) {
      case 4: {
        uint32_t sec;
        _msgpack_load32(uint32_t, o.via.ext.data(), &sec);
        if (sec > std::numeric_limits<time_t>::max()) {
          throw std::range_error("Cannot convert seconds greater than time_t bounds to ptime.");
        }
        v = boost::posix_time::from_time_t((time_t)sec);
      } break;
      case 8: {
          uint64_t value;
          _msgpack_load64(uint64_t, o.via.ext.data(), &value);
          uint32_t nanosec = static_cast<uint32_t>(value >> 34);
          uint64_t sec = value & 0x00000003ffffffffLL;
          if (sec > std::numeric_limits<time_t>::max()) {
            throw std::range_error("Cannot convert seconds greater than time_t bounds to ptime.");
          }
          v = boost::posix_time::from_time_t((time_t)sec);
          v += boost::posix_time::microseconds(nanosec/1000);
      } break;
      case 12: {
          uint32_t nanosec;
          _msgpack_load32(uint32_t, o.via.ext.data(), &nanosec);
          int64_t sec;
          _msgpack_load64(int64_t, o.via.ext.data() + 4, &sec);
          if (sec > std::numeric_limits<time_t>::max() || sec < std::numeric_limits<time_t>::min()) {
            throw std::range_error("Cannot convert seconds greater than time_t bounds to ptime.");
          }
          v = boost::posix_time::from_time_t(static_cast<time_t>(sec));
          v += boost::posix_time::microseconds(nanosec/1000);
      } break;
      default:
          throw msgpack::type_error();
      }
      return o;
  }
};

/**
 * Pack boost ptime as msgpack timestamp (ext type -1).
 *
 * This might be Y2038 safe...?
 */
template <>
struct pack<boost::posix_time::ptime> {
  template <typename Stream>
  msgpack::packer<Stream>& operator()(msgpack::packer<Stream>& o,
                                      const boost::posix_time::ptime& v) const {
    int64_t sec;
    int64_t nanosec;
    if (v.is_not_a_date_time()) {
      sec = 0;
      nanosec = 0;
    } else {
      const boost::posix_time::ptime epoch(boost::gregorian::date(1970,1,1));
      boost::posix_time::time_duration duration (v - epoch);
      sec = duration.total_seconds();
      nanosec = duration.fractional_seconds()*1000;
    }
    if ((sec >> 34) == 0) {
      uint64_t data64 = (static_cast<uint64_t>(nanosec) << 34) | static_cast<uint64_t>(sec);
      if ((data64 & 0xffffffff00000000L) == 0) {
          // timestamp 32
          o.pack_ext(4, -1);
          uint32_t data32 = static_cast<uint32_t>(data64);
          char buf[4];
          _msgpack_store32(buf, data32);
          o.pack_ext_body(buf, 4);
      }
      else {
        // timestamp 64
        o.pack_ext(8, -1);
        char buf[8];
        _msgpack_store64(buf, data64);
        o.pack_ext_body(buf, 8);
      }
    }
    else {
      // timestamp 96
      o.pack_ext(12, -1);
      char buf[12];
      _msgpack_store32(&buf[0], static_cast<uint32_t>(nanosec));
      _msgpack_store64(&buf[4], sec);
      o.pack_ext_body(buf, 12);
    }
    return o;
  }
};

#endif /* HPX_STR_TIMESTAMP */

} // namespace adaptor
} // MSGPACK_API_VERSION_NAMESPACE(MSGPACK_DEFAULT_API_NS)
} // namespace msgpack


#endif /* SRC_ADAPTORS_HPP_ */
