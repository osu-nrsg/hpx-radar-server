/**
 * @file HPxStatus.hpp
 * @brief Definition of the HPxStatus message class.
 *
 * @date Jan 2, 2019
 * @author Randall Pittman <Randall.Pittman@oregonstate.edu>
 * @copyright MIT (see LICENSE.txt)
 *
 * @note The MSGPACK_DEFINE_MAP macro creates the necessary bits of code so that msgpack knows how to pack
 *       an instance of this class as a MessagePack map (key-value pairs) where the name of each attribute
 *       below is the key and the value of the attribute is the value. This allows for a little better
 *       transparency with clients as each field is named.
 */

#ifndef SRC_HPXSTATUS_HPP_
#define SRC_HPXSTATUS_HPP_

#include <string>
#include <msgpack.hpp>
#include <SPxLibData/SPxHPx100Source.h>
#include "ServerState.hpp"

// Add MSGPACK conversion for the enum
MSGPACK_ADD_ENUM(ServerState);

/**
 * Class to get status info and pack into a msgpack buffer.
 *
 * The fields are packed into the buffer on object construction. Create a new object each time you wish to
 * send a new status message.
 */
class HPxStatus {
 public:
  /**
   * Construct a new HPxStatus object.
   */
  HPxStatus() {}
  /**
   * Pack the buffer.
   *
   * Gets some stats from the DAQ card and packs them into the buffer.
   *
   * @param current_state Current state of the HPxServer state machine.
   * @param hpx_src       HPxSPx100Source object to get stats.
   * @param runtime_sec   How long hpx-radar-server has been running.
   * @param message       Reason for degraded or error state, if any.
   *
   */
  void pack(ServerState current_state, SPxHPx100Source & hpx_src, unsigned int runtime_sec,
            unsigned int rays_last_rot, std::string message = "");
  /// Current server state.
  ServerState state = ServerState::begin;
  /// AveragePRFInstantaneous
  double avg_prf = 0;
  /// Number of ACPs in the last rotation
  unsigned int meas_azimuths = 0;
  /// Number of rays in last rotation
  unsigned int meas_rays = 0;
  /// Number of seconds the system has been running
  unsigned int runtime_s = 0;
  /// Additional status information, such as an error message.
  std::string message = "";
  /// msgpack simple buffer
  msgpack::sbuffer sbuf;
  /// msgpack macro for packing a msgpack map from this class.
  MSGPACK_DEFINE_MAP(state, avg_prf, meas_azimuths, meas_rays, runtime_s, message);
};

#endif /* SRC_HPXSTATUS_HPP_ */
