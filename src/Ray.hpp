/**
 * @file Ray.hpp
 * @brief Definition of Ray class.
 *
 * @date Oct 25, 2019
 * @author Randall Pittman <Randall.Pittman@oregonstate.edu>
 * @copyright MIT (see LICENSE.txt)
 */

#ifndef RAY_HPP_
#define RAY_HPP_

// 3rd-party
#include <boost/date_time/posix_time/posix_time.hpp>
#include <msgpack.hpp>
#include <SPxLibUtils/SPxTypes.h>

// project
#include "util/adaptors.hpp"
#include "util/errors.hpp"

/**
 * Class for holding radar ray data and header values.
 *
 * Ray objects are intended to be reused. When a Ray is created, a buffer of `max_data_nbytes` is allocated
 * for the lifetime of the Ray. Data is copied into this buffer (accessed via )
 *
 */
class Ray {
 public:
  using ptime = boost::posix_time::ptime;
  /// Default constructor - Create an empty ray.
  Ray();
  /// Constructor - Create a ray preallocated with max_data_nbytes
  Ray(size_t max_data_nbytes);
  /// Set all fields to zero and set data to be empty.
  void reset();
  /// Set the fields of the ray
  void set_fields(unsigned int rotation_idx, unsigned int ray_idx, unsigned int memory_bank, ptime proc_time,
                  UINT16 time_interval, UINT16 azimuth, UCHAR packing,
                  const unsigned char* const data_ptr, size_t data_nbytes);
  /// Pack the ray into sbuf.
  void pack();
  /// rotation index
  unsigned int rotation_idx;
  /// ray index (per rotation)
  unsigned int ray_idx;
  /// HPx card memory bank this ray was in
  unsigned int memory_bank;
  /// System time when processing began on this array
  ptime proc_time;
  /// Time in microseconds since the last ray
  UINT16 time_interval;
  /// Calculated true time that the ray was received
  ptime calc_time;
  /// Azimuth count 0...65535
  UINT16 azimuth;
  /// SPX_RIB_PACKING_* data packing type. Not included in packed RAY message.
  UCHAR packing;
  /// The data array. See RayArray docs.
  RawArray data;
  /// Macro to create a msgpack map message from this class when packed.
  MSGPACK_DEFINE_MAP(rotation_idx, ray_idx, memory_bank, proc_time, time_interval, calc_time, azimuth, data);
  /// Reusable simple buffer for msgpack packing.
  msgpack::sbuffer sbuf;
  /// Quick check to see if the ray is filtered.
  bool is_filtered();
};

#endif /* RAY_HPP_ */
