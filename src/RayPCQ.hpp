/**
 * @file RayPCQ.hpp
 * @brief Definition and implementation of the RayPCQ class.
 *
 * @date Feb 18, 2019
 * @author Randall Pittman <Randall.Pittman@oregonstate.edu>
 * @copyright MIT (see LICENSE.txt)
 *
 */

#ifndef SRC_UTIL_LOCKINGQUEUE_HPP_
#define SRC_UTIL_LOCKINGQUEUE_HPP_

#include <condition_variable>  // includes <chrono>
#include <mutex>
#include <queue>
#include "RayPool.hpp"

/**
 * Thread-safe queue class for passing ray pointers from the producer to the consumer.
 */
class RayPCQ {
 /// rayptr_t is a unique_ptr with a special deleter, and is defined in RayPool.
 using rayptr_t = RayPool::ptr_type;
 private:
  std::queue<rayptr_t> queue;
  mutable std::mutex guard;
  std::condition_variable rays_ready;

 public:
  /**
   * Push a ray pointer to the queue.
   * Only accept rvalue refs since ray pointers are always moved, not copied.
   * Acquires a lock to push the pointer on to the queue, then sets the conditional variable to indicate that
   * there is data on the queue to consume.
   *
   * @param rayptr  Ray pointer to push onto the back of the queue
   */
  void push(rayptr_t&& rayptr) {
    {
      std::lock_guard<std::mutex> lock(guard);
      queue.push(std::move(rayptr));
    }
    rays_ready.notify_one();
  }
  /// Report if the queue is empty.
  bool empty() const {
    // lock guard doesn't really make sense here, as the queue could become non-empty right after the
    // function call completes.
    return queue.empty();
  }
  /**
   * Attempt to get a pointer from the front of the queue and move it into rayptr, then pop the now-empty
   * pointer off the queue.
   * @param rayptr  Uninitialized rayptr_t to move the pointer into.
   * @return bool  True if a pointer was moved to rayptr, false if the queue was empty.
   */
  bool tryPop(rayptr_t& rayptr) {
    std::lock_guard<std::mutex> lock(guard);
    if (queue.empty()) {
      return false;
    }
    rayptr = std::move(queue.front());
    queue.pop();
    return true;
  }

  /**
   * Wait for a pointer to be put into the queue and move it into rayptr.
   * (Same as tryPop but with a timeout to wait for something to be available in the queue.)
   * @param rayptr  Uninitialized rayptr_t to move the pointer into.
   * @param max_wait  If the queue is empty, max time to wait for an entry in the queue.
   * @return bool  True if a pointer was moved to rayptr, false if the queue was empty and no pointer was
   *               put in the queue before the timeout.
   */
  template<typename _Rep, typename _Period>
  bool waitAndPop(rayptr_t& rayptr, const std::chrono::duration<_Rep, _Period>& max_wait) {
    /*
     * If the queue is empty wait max_wait for a ray to be ready to pop before timing out.
     * We use wait_until() rather than wait_for() so the entire function times-out after max_wait,
     * even with spurious wakeups.
     * Duplicated the duration casting from the GNU condition_variable.wait_for() code.
     */
    using __steady_clock_t = std::chrono::steady_clock;
    auto wait_duration = std::chrono::duration_cast<__steady_clock_t::duration>(max_wait);
    if (wait_duration < max_wait)
      ++wait_duration;
    auto wait_until_time = __steady_clock_t::now() + wait_duration;

    std::unique_lock<std::mutex> lock(guard);
    while (queue.empty()) {
      if (rays_ready.wait_until(lock, wait_until_time) == std::cv_status::timeout) {
        // timeout exceeded, nothing to return
        return false;
      }
      // HERE - either spurious wakeup or there's something to get. Loop again to check.
    }
    rayptr = std::move(queue.front());
    queue.pop();
    return true;
  }
};

#endif /* SRC_UTIL_LOCKINGQUEUE_HPP_ */
