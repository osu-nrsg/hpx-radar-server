/**
 * @file SPxErrorHandler.hpp
 * @brief Definition and implementation of the SPxErrorHandler class.
 *
 * @date Oct 12, 2018
 * @author Randall Pittman <Randall.Pittman@oregonstate.edu>
 * @copyright MIT (see LICENSE.txt)
 */

#ifndef SRC_SPXERRORHANDLER_HPP_
#define SRC_SPXERRORHANDLER_HPP_

#include <string>
#include <boost/format.hpp>
#include <SPxLibData/SPxHPx100Source.h>
#include "util/errors.hpp"

// FUTURE: Remove boost dependency
/**
 * Basic class for handling SPx library errors.
 *
 * This used to be more complex to send out a special 0MQ message but now all error states are reported
 * in the STATUS message.
 */
class SPxErrorHandler {
 public:
  /**
   * Generate an error string, log it, and send it out as an SPXERROR ZeroMQ message.
   *
   * Not sure of the meaning of args 1-4.
   * This should be called by the error handling function installed to SPxHPx100Source.
   */
  void handle_error(SPxErrorType errType, SPxErrorCode errCode,
                    int arg1, int arg2, const char *arg3, const char *arg4) {
    using std::to_string;
    using std::string;
    // Build error string
    string err_msg = boost::str(boost::format("Severity: %s, Args: %d, %d, %s, %s") %
                                string(SPxGetErrorSeverityString(errType)) %
                                arg1 %
                                arg2 %
                                (arg3 ? arg3 : "<none>") %
                                (arg4 ? arg4 : "<none>"));
    switch (errType) {
      // Non-error errors
      case SPxErrorType::SPX_ERR_WARNING:
        SPDLOG_WARN("SPx warning msg " + to_string(errCode) + ": " +
                    string(SPxGetErrorStringDetail(errCode)));
        break;
      case SPxErrorType::SPX_ERR_INFO:
        SPDLOG_INFO("SPx info msg " + to_string(errCode) + ": " +
                 string(SPxGetErrorStringDetail(errCode)));
        break;
      // all other errors are "real" errors.
      default:
        throw HPxSPxError(errCode, err_msg);
    }
  }
};

/**
 * Error handling function to install to the SPx library.
 *
 * Uses SPxErrorHandler to handle the errors.
 *
 * @param errType - SPxErrorType (See SPxError.h)
 * @param errCode - SPxErrorCode (See SPxError.h)
 * @param arg1 - unknown
 * @param arg2 - unknown
 * @param arg3 - unknown
 * @param arg4 - unknown
 * @param user_arg - SPxErrorHandler* to use to handle the error.
 */
inline void handle_spx_errors(
    SPxErrorType errType, SPxErrorCode errCode,
    int arg1, int arg2, const char *arg3, const char *arg4, void * user_arg) {
  if (eptrs.spxerr_thread) {
    // Don't try to do anything if this thread has errored.
    return;
  }
  try {
    SPxErrorHandler* err_handler_ptr = static_cast<SPxErrorHandler *>(user_arg);
    err_handler_ptr->handle_error(errType, errCode, arg1, arg2, arg3, arg4);
  } catch (...) {
    eptrs.spxerr_thread = std::current_exception();
    SPDLOG_ERROR("SPx error: " + what(eptrs.spxerr_thread));
  }
}


#endif /* SRC_SPXERRORHANDLER_HPP_ */
