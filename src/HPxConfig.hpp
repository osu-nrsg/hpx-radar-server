/**
 * @file HPxConfig.hpp
 * @brief Declaration of the HPxConfig message class.
 *
 * @date Jan 2, 2019
 * @author Randall Pittman <Randall.Pittman@oregonstate.edu>
 * @copyright MIT (see LICENSE.txt)
 *
 * @note The MSGPACK_DEFINE_MAP macro creates the necessary bits of code so that msgpack knows how to pack
 *       an instance of this class as a MessagePack map (key-value pairs) where the name of each attribute
 *       below is the key and the value of the attribute is the value. This allows for a little better
 *       transparency with clients as each field is named.
 */


#ifndef SRC_HPXCONFIG_HPP_
#define SRC_HPXCONFIG_HPP_

#include <map>
#include <stdexcept>
#include <string>
#include <msgpack.hpp>
#include <SPxLibData/SPxHPx100Source.h>

#include "version.h"
#include "util/logging.hpp"

/// Possible modes for the TPG
enum class TpgMode {
  analog_ramp = 0,
  digital_pulse,
  digital_ramp,
  offset_dac,
};

// Add MSGPACK conversion for the enum
MSGPACK_ADD_ENUM(TpgMode);

/**
 * Class for parsing, storing, and packing (into msgpack buffer) the HPx card configuration.
 *
 * Only one HPxConfig object should be created and then reused throughout the application.
 */
class HPxConfig {
 public:
  /**
   * Construct a new HPxConfig object.
   *
   * @param config_path Filesystem path to a config TOML file. Provide empty string to only use config_toml.
   * @param config_toml String of valid TOML with extra config params. Takes precedence over the file at
   *                    config_path.
   */
  HPxConfig(std::string config_path, std::string config_toml = "");
  /// Default Constructor. Leaves the config in an invalid state (not configured).
  HPxConfig();
  /// Try parsing the toml file at config_path and/or config_toml. Check validity with is_valid().
  void load_config();
  std::string config_path;
  std::string config_toml;
  // Settable parameters. Don't manually change these unless you're certain the values are valid.
  unsigned int board_idx = 0;
  char analog_channel = 'A';
  unsigned int num_samples = 1024;
  double start_range_metres = 0;
  double range_correction_metres = 0;
  double voltage_offset = 0;
  double gain = 1;
  bool tpg_enabled = false;
  TpgMode tpg_mode = TpgMode::analog_ramp;
  double tpg_trigger_frequency = 2000.0;
  double tpg_acp_frequency = 2560.0;  // 2048 ACP/rot @ 48 RPM
  unsigned int tpg_arp_frequency = 2048;  // number of ACP/ARP
  bool enable_azimuth_interpolation = true;
  unsigned int status_and_config_period_ms = 500;
  int spokes_per_interrupt = -1;  // -1 means unset
  int interrupts_per_second = -1;  // -1 means unset
  bool calc_true_timestamps = true;
  unsigned int summing_factor = 1;
  UINT16 start_azival = 0;
  UINT16 end_azival = 0;
  bool send_filtered = true;
  // the following parameters are retrieved from the card and should not be set manually
  std::string card_name;
  UINT32 fpga_version;
  double calc_start_range_metres;
  double calc_end_range_metres;  // Calculated value retrieved from GetEndRangeMetres()
  unsigned int meas_spokes_per_interrupt; // Should be set just before sending out config
  // Current software version
  std::string version = HPXRADARSERVER_VERSION_STRING;

  /// Msgpack buffer for output.
  msgpack::sbuffer sbuf;
  /// Msgpack macro for packing a msgpack map from this class.
  MSGPACK_DEFINE_MAP(\
      board_idx, analog_channel, num_samples, start_range_metres, range_correction_metres,\
      voltage_offset, gain, tpg_enabled, tpg_mode, tpg_trigger_frequency, tpg_acp_frequency,\
      tpg_arp_frequency, enable_azimuth_interpolation, status_and_config_period_ms, \
      spokes_per_interrupt, interrupts_per_second, calc_true_timestamps, summing_factor, \
      start_azival, end_azival, send_filtered, card_name, fpga_version, calc_start_range_metres, \
      calc_end_range_metres, meas_spokes_per_interrupt, version);
  /// Get current config params and pack this class into sbuf.
  void pack(SPxHPx100Source & hpx_src);
  /// Check if the config is valid. Put any config error message in err_str, if supplied.
  bool is_valid();
  std::string invalid_reason();
  /**
   * Print the config to the log.
   *
   * @param level spdlog log level
   */
  void log(loglevel::level_enum level);
 private:
  /// Parse the TOML file from config_path into this class.
  void parse_file();
  /// Parse the TOML text from config_toml into this class.
  void parse_config();
  bool valid;
  std::string error_message;
//  loglevel::level_enum loglevel = loglevel::info;
};

/// Map of TPG modes to strings
const std::map<TpgMode, std::string> tpg_mode_strs = {
  {TpgMode::analog_ramp, "analog_ramp"},
  {TpgMode::digital_pulse, "digital_pulse"},
  {TpgMode::digital_ramp, "digital_ramp"},
  {TpgMode::offset_dac, "offset_dac"},
};

/// Get TPG mode from string (invert tpg_mode_strs map)
inline TpgMode get_tpg_mode(std::string mode_str) {
  for (auto it=tpg_mode_strs.begin(); it!=tpg_mode_strs.end(); ++it) {
    if (it->second == mode_str) return it->first;
  }
  throw std::invalid_argument("No TPG mode matching string " + mode_str);
}

/// return all tpg modes as a ", "-concatenated string
inline std::string tpg_modes() {
  std::string out;
  for (auto it=tpg_mode_strs.begin(); it!=tpg_mode_strs.end(); ++it) {
    if (it != tpg_mode_strs.begin()) out.append(", ");
    out.append(it->second);
  }
  return out;
}


#endif /* SRC_HPXCONFIG_HPP_ */
