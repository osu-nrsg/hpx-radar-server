/**
 * @file HPxConfig.hpp
 * @brief Definitions for the HPxConfig message class.
 *
 * @date Jan 2, 2019
 * @author Randall Pittman <Randall.Pittman@oregonstate.edu>
 * @copyright MIT (see LICENSE.txt)
 *
 */


#include <HPxConfig.hpp>
#include <memory>
#include <cpptoml.h>
#include "util/util.hpp"

using std::string;
using std::to_string;

static void process_toml_config(std::shared_ptr<cpptoml::table> toml_conf, HPxConfig & hpx_config);

HPxConfig::HPxConfig(string config_path, string config_toml)
    : config_path(config_path), config_toml(config_toml) {
  load_config();
}

HPxConfig::HPxConfig() {
  error_message = "Not configured.";
  valid = false;
  card_name = "";
  fpga_version = 0;
  calc_start_range_metres = -9999;
  calc_end_range_metres = -9999;
  meas_spokes_per_interrupt = 0;
}

void HPxConfig::load_config() {
  error_message = "";
  valid = true;
  if (config_path == "") {
    error_message = "No configuration file supplied.";
    valid = false;
    return;
  }
  try {
    parse_file();
  } catch (...) {
    error_message = "Error loading config file " + config_path + ": " + what(std::current_exception());
    valid = false;
    return;
  }
  if (config_toml != "") {
    try {
      parse_config();
    } catch (...) {
      if (error_message != "") error_message += "\n";
      error_message += ("Error loading config TOML string " + config_toml + ": " +
                        what(std::current_exception()));
      valid = false;
    }
  }
}

void HPxConfig::pack(SPxHPx100Source & hpx_src) {
  // Update dynamic config variables
  card_name = hpx_src.GetCardName();
  fpga_version = hpx_src.GetFpgaVersion();
  calc_start_range_metres = hpx_src.GetStartRangeMetres();
  calc_end_range_metres = hpx_src.GetEndRangeMetres();
  meas_spokes_per_interrupt = hpx_src.GetSpokesPerInterrupt();

  // Clear and repack the msgpack buffer.
  sbuf.clear();
  msgpack::pack(sbuf, *this);
}

bool HPxConfig::is_valid() {
  return valid;
}

std::string HPxConfig::invalid_reason() {
  return error_message;
}

void HPxConfig::log(loglevel::level_enum level) {
  std::stringstream ss;
  SPDLOG_LOG(level,
    "--CONFIG--\n"
    "board_idx = {}\n"
    "analog_channel = '{:c}'\n"
    "num_samples = {}\n"
    "start_range_metres = {}\n"
    "range_correction_metres = {}\n"
    "voltage_offset = {}\n"
    "gain = {}\n"
    "tpg_enabled = {}\n"
    "tpg_mode = {}\n"
    "tpg_trigger_frequency = {}\n"
    "tpg_acp_frequency = {}\n"
    "tpg_arp_frequency = {}\n"
	"status_and_config_period_ms = {}\n"
    "enable_azimuth_interpolation = {}\n"
    "calc_true_timestamps = {}\n"
    "summing_factor = {}\n"
    "spokes_per_interrupt = {}\n"
    "interrupts_per_second = {}\n"
    "start_azival = {}\n"
    "end_azival = {}\n"
    "send_filtered = {}\n"
    "--END CONFIG--",
	board_idx, analog_channel, num_samples, start_range_metres, range_correction_metres, voltage_offset, gain,
  tpg_enabled, tpg_mode, tpg_trigger_frequency, tpg_acp_frequency, tpg_arp_frequency,
  (enable_azimuth_interpolation ? "true" : "false"), status_and_config_period_ms,
	(calc_true_timestamps ? "true" : "false"), summing_factor, spokes_per_interrupt, interrupts_per_second,
  start_azival, end_azival, (send_filtered ? "true" : "false"));
}

void HPxConfig::parse_file() {
  auto config = cpptoml::parse_file(config_path);
  process_toml_config(config, *this);
}

void HPxConfig::parse_config() {
  std::stringstream ss;
  ss << config_toml;
  cpptoml::parser p(ss);
  process_toml_config(p.parse(), *this);
}

// The use of a separate function allows us to keep cpptoml contained to this source file.
static void process_toml_config(std::shared_ptr<cpptoml::table> toml_conf, HPxConfig & hpx_config){
  // Process the options
  std::shared_ptr<cpptoml::table> board;
  std::shared_ptr<cpptoml::table> sampling;
  std::shared_ptr<cpptoml::table> leveling;
  std::shared_ptr<cpptoml::table> tpg;
  std::shared_ptr<cpptoml::table> misc;
  std::shared_ptr<cpptoml::table> buffers;
  std::shared_ptr<cpptoml::table> filtering;
  cpptoml::option<unsigned int> uival;
  cpptoml::option<UINT16> u16val;
  cpptoml::option<char> chval;
  cpptoml::option<double> dblval;
  cpptoml::option<int> ival;
  cpptoml::option<bool> bval;
  cpptoml::option<string> strval;

  // [Board]
  board = toml_conf->get_table("Board");
  if (board) {
    uival = board->get_qualified_as<unsigned int>("board_idx");
    if (uival) hpx_config.board_idx = *uival;

    strval = board->get_qualified_as<string>("analog_channel");
    if (strval) {
      if (*strval != "A" && *strval != "B") {
        throw ConfigError("Analog channel must be 'A' or 'B'.");
      }
      hpx_config.analog_channel = (*strval)[0];
    }
  }

  // [Sampling]
  sampling = toml_conf->get_table("Sampling");
  if (sampling) {
    uival = sampling->get_qualified_as<unsigned int>("num_samples");
    if (uival) hpx_config.num_samples = *uival;

    dblval = sampling->get_qualified_as<double>("start_range_m");
    if (dblval) hpx_config.start_range_metres = *dblval;

    dblval = sampling->get_qualified_as<double>("range_correction_m");
    if (dblval) hpx_config.range_correction_metres = *dblval;
  }

  // [Leveling]
  leveling = toml_conf->get_table("Leveling");
  if (leveling) {
    dblval = leveling->get_qualified_as<double>("voltage_offset");
    if (dblval) hpx_config.voltage_offset = *dblval;

    dblval = leveling->get_qualified_as<double>("gain");
    if (dblval) hpx_config.gain = *dblval;
  }

  // [TPG]
  tpg = toml_conf->get_table("TPG");
  if (tpg) {
    bval = tpg->get_qualified_as<bool>("enabled");
    if (bval) hpx_config.tpg_enabled = *bval;

    strval = tpg->get_qualified_as<string>("mode");
    if (strval) {
      try {
        hpx_config.tpg_mode = get_tpg_mode(*strval);
      } catch (std::invalid_argument &e) {
        throw ConfigError("TPG mode must be one of " + tpg_modes());
      }
    }

    dblval = tpg->get_qualified_as<double>("trigger_frequency");
    if (dblval) hpx_config.tpg_trigger_frequency = *dblval;

    dblval = tpg->get_qualified_as<double>("acp_frequency");
    if (dblval) hpx_config.tpg_acp_frequency = *dblval;

    uival = tpg->get_qualified_as<unsigned int>("arp_frequency");
    if (uival) hpx_config.tpg_arp_frequency = *uival;
  }

  // [Misc]
  misc = toml_conf->get_table("Misc");
  if (misc) {
	uival = misc->get_qualified_as<unsigned int>("status_and_config_period_ms");
	if (uival) hpx_config.status_and_config_period_ms = *uival;

    bval = misc->get_qualified_as<bool>("enable_azimuth_interpolation");
    if (bval) hpx_config.enable_azimuth_interpolation = *bval;

    bval = misc->get_qualified_as<bool>("calc_true_timestamps");
    if (bval) hpx_config.calc_true_timestamps = *bval;

    uival = misc->get_qualified_as<unsigned int>("summing_factor");
    if (uival) hpx_config.summing_factor = *uival;
  }
  if (hpx_config.summing_factor < 1 || hpx_config.summing_factor > 16) {
    throw ConfigError("Misc.summing_factor must be in range [1, 16].");
  }

  // [Buffers]
  buffers = toml_conf->get_table("Buffers");
  if (buffers) {
    ival = buffers->get_qualified_as<int>("spokes_per_interrupt");
    if (ival) hpx_config.spokes_per_interrupt = *ival;

    ival = buffers->get_qualified_as<int>("interrupts_per_second");
    if (ival) hpx_config.interrupts_per_second = *ival;
  }
  if (hpx_config.spokes_per_interrupt > -1 && hpx_config.interrupts_per_second > -1) {
    throw ConfigError("spokes_per_interrupt and interrupts_per_second may not be both set.");
  }

  // [Filtering]
  filtering = toml_conf->get_table("Filtering");
  if (filtering) {
    u16val = filtering->get_qualified_as<UINT16>("start_azival");
    if (u16val) hpx_config.start_azival = *u16val;

    u16val = filtering->get_qualified_as<UINT16>("end_azival");
    if (u16val) hpx_config.end_azival = *u16val;

    bval = filtering->get_qualified_as<bool>("send_filtered");
    if (bval) hpx_config.send_filtered = *bval;
  }
}
