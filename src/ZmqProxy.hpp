/**
 * @file ZmqProxy.hpp
 * @brief Contains the ZmqProxy class.
 *
 * @date Dec 26, 2019
 * @author Randall Pittman <Randall.Pittman@oregonstate.edu>
 * @copyright MIT (see LICENSE.txt)
 */

// FUTURE: Possibly re-make the proxy function itself to allow quitting by an atomic_bool?

#ifndef SRC_ZMQPROXY_HPP_
#define SRC_ZMQPROXY_HPP_

#include <atomic>
#include <exception>
#include <thread>
#include "zmq.hpp"
#include "util/util.hpp"

/// XSUB/XPUB proxy to pass messages from multiple publishers to multiple subscribers via a single socket.
class ZmqProxy {
 public:
  /**
   * Construct a ZmqProxy object and start the proxy thread.
   *
   * @param context - ZeroMQ context for the sockets.
   * @param xsub_endpoint - ZeroMQ endpoint for the proxy to listen on. All external publishers publish to
   *                        this endpoint. Note: The proxy binds this endpoint so all external publishers
   *                        should connect and not bind to this endpoint.
   * @param xpub_endpoint - ZeroMQ endpoint for the proxy to publish to. All external subscribers listen to
   *                        this endpoint.
   * @param rethrow_err_on_destroy - If true, any error in the proxy will be rethrown in the calling thread
   *                                 when the ZmqProxy object is destroyed. Default is false.
   */
  ZmqProxy(zmq::context_t & context, std::string xsub_endpoint, std::string xpub_endpoint,
           bool rethrow_err_on_destroy = false)
       : context(context), xsub_endpoint(xsub_endpoint), xpub_endpoint(xpub_endpoint),
         rethrow_err_on_destroy(rethrow_err_on_destroy) {
    running = false;
    start();
  }

  /// Try to shut down the proxy and join the thread.
  ~ZmqProxy() {
    SPDLOG_TRACE("Destroying ZmqProxy");
    if (proxy_thread.joinable()) {
      stop();
      proxy_thread.join();
      SPDLOG_TRACE("0MQ proxy thread joined.");
    }
    if (rethrow_err_on_destroy && eptr) {
      std::rethrow_exception(eptr);
    }
  }

  /// Start the proxy thread.
  void start() {
    if (proxy_thread.joinable()) {
      throw ZmqProxyErr("Proxy is already started.");
    }
    proxy_thread = std::thread(&ZmqProxy::run_proxy_wrapper, this);
  }

  /// Send a stop command to stop the proxy.
  void stop() {
    zmq::socket_t ctl_pub(context, ZMQ_PUB);
    ctl_pub.bind(control_endpoint);
    ctl_pub.set(zmq::sockopt::linger, 0);
    ctl_pub.send(zmq::str_buffer("TERMINATE"), zmq::send_flags::none);
    ctl_pub.close();
    SPDLOG_TRACE("Send TERMINATE to proxy.");
  }

  /// Getter for `running` member.
  bool is_running() {
    return running;
  }

  /// Store point for any exception in the proxy to be handled outside the ZmqProxy object.
  std::exception_ptr eptr;
 private:
  zmq::context_t& context;
  std::string xsub_endpoint;
  std::string xpub_endpoint;
  /// ZeroMQ endpoint to use for controlling the proxy.
  std::string control_endpoint = "inproc://zmqproxy-ctl";
  bool rethrow_err_on_destroy;
  std::atomic_bool running;
  std::thread proxy_thread;

  /// Wrapper for the run_proxy function that puts any exceptions in the exception_ptr for later handling.
  void run_proxy_wrapper() {
    try {
      run_proxy();
    } catch (...) {
      eptr = std::current_exception();
      SPDLOG_ERROR("Proxy error: " + what(eptr));
    }
    running = false;
  }

  /// Main function to run the proxy. Captures all messages in the XSUB socket and forwards them out to the
  /// XPUB socket.
  void run_proxy() {
    zmq::socket_t xsub(context, ZMQ_XSUB);
    zmq::socket_t xpub(context, ZMQ_XPUB);
    zmq::socket_t ctl_sub(context, ZMQ_SUB);
    xsub.bind(xsub_endpoint);
    xpub.bind(xpub_endpoint);
    ctl_sub.connect(control_endpoint);
    ctl_sub.set(zmq::sockopt::subscribe, "");
    xsub.set(zmq::sockopt::linger, 0);
    xpub.set(zmq::sockopt::linger, 0);
    ctl_sub.set(zmq::sockopt::linger, 0);

    SPDLOG_TRACE("0MQ proxy XSUB @ {}, XPUB @ {}, ctl_SUB @ {}", control_endpoint, xsub_endpoint, xpub_endpoint, control_endpoint);
    running = true;
    // Run the proxy until 'TERMINATE' is received on the control socket or the context is terminated.
    try {
      zmq::proxy_steerable(xsub, xpub, zmq::socket_ref(), ctl_sub);
    } catch (zmq::error_t &err) {
      if (err.num() == ETERM) {
        // If the context is closed before "TERMINATE" is recived on the control socket then 0MQ throws an
        // ETERM error.
        SPDLOG_TRACE("0MQ proxy - context closed.");
      } else {
        std::throw_with_nested(HPxServerError("Error in 0MQ proxy."));
      }
    }
    // Don't hold the sockets open to communicate remaining messages.
    xsub.set(zmq::sockopt::linger, 0);
    xpub.set(zmq::sockopt::linger, 0);
    ctl_sub.set(zmq::sockopt::linger, 0);
    xsub.close();
    xpub.close();
    ctl_sub.close();
  }
};

#endif /* SRC_ZMQPROXY_HPP_ */
