/**
 * @file hpx-radar-server.cpp
 * @brief Entry point for the HPx Radar Server application.
 *
 * @date Sep 26, 2018
 * @author Randall Pittman <Randall.Pittman@oregonstate.edu>
 * @copyright MIT (see LICENSE.txt)
 */

#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <string>
#include <unistd.h>
#include <sys/types.h>
#include "HPxServer.hpp"  // HPxServer class
#include "util/util.hpp"
#include "version.h"

using std::string;
using std::to_string;

#define EXIT_INVALIDARGUMENT 2
#define EXIT_NOTCONFIGURED 6
#define EXIT_NOTRUNNING 7

constexpr loglevel::level_enum default_journald_loglevel =  loglevel::info;
constexpr loglevel::level_enum default_console_loglevel = loglevel::debug;

struct cli_args {
  loglevel::level_enum log_level = loglevel::off;
  bool daemon = false;
  string config_file_path;
  string config_toml;
};

/// Check to see if another instance of the app is running and if so, log an error and return -1.
int check_running_instance();

/**
 * Parse the command-line arguments with getopt and do simple argument checking.
 *
 * @param argc Argument count
 * @param argv CLI arguments
 * @param args Structure of parsed arguments
 * @return 0 Success, -1 error, 1 normal early-exit (e.g. printing version or usage)
 */
int parse_cli(int argc, char* argv[], cli_args& args);

/**
 * Get the ZeroMQ endpoint to use for publishing messages.
 *
 * If the HPXENDPOINT environment variable is set, this is used as the endpoint. Otherwise, the default is
 * to generate an ipc-protocol endpoint at /run/hpx-radar-server/hpx-pub.sock
 *
 * @param[out] pub_endpoint The ZeroMQ endpoint to use
 * @return 0 for success, -1 for error (e.g. cannot acccess the /run/hpx-radar-server dir)
 */
int get_pub_endpoint(string& pub_endpoint);

/**
 * Run the HPx Radar Server application.
 *
 * Command-line arguments are parsed in `parse_cli`.
 *
 * HPXCONFIGPATH, HPXENDPOINT, and HPXCONFIGTOML are retrieved from environment variables. Starts an instance
 * of HPxServer.
 *
 * @param argc Number of command-line arguments
 * @param argv Command-line arguments
 * @return 0 if no error, 1 if error.
 */
int main(int argc, char* argv[]) noexcept {
  int ret;
  cli_args args;
  string pub_endpoint;

  if ((ret = check_running_instance()) != 0) return ret;
  if ((ret = parse_cli(argc, argv, args)) != 0) return ret;
  if (args.daemon) {
    log_init(loglevel::off, args.log_level);
  } else {
    log_init(args.log_level, loglevel::off);
  }
  if ((ret = get_pub_endpoint(pub_endpoint)) != 0) return ret;

  try {
    HPxServer hpx_server(pub_endpoint, args.config_file_path, args.config_toml, args.daemon);
    hpx_server.run_server();
  } catch (...) {
    SPDLOG_ERROR("Error in HPx Radar Server: " + what(std::current_exception()));
    return EXIT_FAILURE;
  }
  return EXIT_SUCCESS;
}

/// Print usage text, along with optional usage error info.
void print_usage(std::string err_text = "") {
  if (err_text != "") {
    std::cerr << "Usage error: " << err_text << "\n"
      "hpx-radar-server -h shows proper usage." << std::endl;
    return;
  }
  std::cerr <<
    "Start the HPx Radar Server.\n"
    "\n"
    "USAGE:\n"
    "\n"
    "  hpx-radar-server [-c config_file] [-t config_toml] [-d] [-v level] [-V] [-h]\n"
    "\n"
    "Note: By default the server will use the ipc:// protocol and attempt to set\n"
    "up unix domain socket at /run/hpx-radar-server/hpx-pub.sock for\n"
    "the ZeroMQ publisher. To specify a different ZeroMQ endpoint, set it in the\n"
    "environment variable HPXENDPOINT.\n"
    "\n"
    "At any time, the application may be stopped by sending SIGTERM (Ctrl-C) or\n"
    "SIGINT.\n"
    "\n"
    "OPTIONS:\n"
    " -c CONFIG_FILE\n"
    "    Path to a TOML-formatted configuration file with the server's\n"
    "    configuration. If not specified, /etc/hpx-radar-server.toml is used, if\n"
    "    available.\n"
    " -t \"CONFIG_TOML\"\n"
    "    Optional TOML-formatted string containing command-line overrides of the\n"
    "    values in the config file.\n"
    " -d\n"
    "    Enable daemon functionality. Log content will be written to journald\n"
    "    rather than the console. If run as a service, systemd will be notified of\n"
    "    process state and status with `sd_notify` calls. If SIGHUP is sent, the\n"
    "    application will reload its configuration from the config file (and the\n"
    "    config TOML string, if specified with option `-t`).\n"
    " -v LEVEL\n"
    "    Set log level to LEVEL.\n"
    "    Log levels are 0:trace 1:debug 2:info 3:warning 4:error 5:critical 6:off\n"
    "    Default level is " << default_journald_loglevel << " for daemon, "
      << default_console_loglevel << " for non-daemon.\n" <<
    " -V\n"
    "    Print version to console and exit.\n"
    " -h\n"
    "    Print this message.\n"
    "\n"
    "EXAMPLES\n"
    "  # Look for hpx-radar-server.toml config file in /etc or ~/.config, and set\n"
    "  # verbosity level to TRACE.\n"
    "  hpx-radar-server -v 0\n"
    "\n"
    "  # Enable daemon mode and use the config file at configs/test_config.toml\n"
    "  hpx-radar-server -d -c configs/test_config.toml\n"
    "\n"
    "  # Apply some modifications to the config (TOML is newline-delimited):\n"
    "  CONFIG=\"\n"
    "  TPG.enabled = true\n"
    "  TPG.trigger_frequency = 3000\"\n"
    "  hpx-radar-server -c configs/test_config.toml -t \"$CONFIG\"\n"
    "\n"
    "  # Print version number and exit:\n"
    "  hpx-radar-server -V\n"
    "\n"
    "  # Print this text and exit:\n"
    "  hpx-radar-server -h\n"
    << std::endl;
}

int check_running_instance() {
  // See if the hpx-radar-server is running and if so, under which user.
  char pgrep_result[32] = "";
  char ps_result[32] = ""; // ps -o user= -p
  char* ret;
  int other_pid = -1;
  pid_t my_pid = getpid();
  FILE* fp;
  fp = popen("pgrep hpx-radar-serve", "r");
  while (true) {
    if (fgets(pgrep_result, sizeof(pgrep_result), fp) == NULL) break;
    other_pid = atoi(pgrep_result);
    if (other_pid != my_pid) break;
    pgrep_result[0] = '\0';
  }
  pclose(fp);
  if (other_pid != -1 && other_pid != my_pid) {
    string ps_cmd("ps -o user= -p " + to_string(other_pid));  // use string for simple concatenation.
    fp = popen(ps_cmd.c_str(), "r");
    ret = fgets(ps_result, sizeof(ps_result), fp);
    pclose(fp);
    if (ret != NULL) {
      SPDLOG_ERROR("hpx-radar-server is already running on this system with pid {} under username {}",
                   other_pid, ps_result);
      return EXIT_FAILURE;
    }
  }
  return 0;
}

int parse_cli(int argc, char* argv[], cli_args& args) {
  int log_level = -1;
  char opt;
  while((opt = getopt(argc, argv, "c:t:dv:Vh")) != -1) {
    switch (opt) {
      case 'c':
        args.config_file_path = optarg;
        break;
      case 't':
        args.config_toml = optarg;
        break;
      case 'd':
        args.daemon = true;
        break;
      case 'v':
        try {
          log_level = std::stoi(optarg);
          if (log_level < loglevel::trace || log_level > loglevel::off) {
            throw std::invalid_argument("Invalid log level specified.");
          }
        } catch (...) {
          print_usage(what(std::current_exception(), false));
          return EXIT_INVALIDARGUMENT;
        }
        break;
      case 'V':
        std::cout << HPXRADARSERVER_VERSION_STRING << std::endl;
        return EXIT_NOTRUNNING;
      case 'h':  // fallthrough
        print_usage(); // early exit
        return EXIT_NOTRUNNING;
      default: /* '?' */
        print_usage("Invalid argument specified."); // early exit.
        return EXIT_INVALIDARGUMENT;
    }
  }
  if (log_level >= 0) {
    args.log_level = static_cast<loglevel::level_enum>(log_level);
  } else {
    args.log_level = (args.daemon) ? default_journald_loglevel : default_console_loglevel;
  }
  return 0;
}

int get_pub_endpoint(string& pub_endpoint) {
  pub_endpoint = get_env_str("HPXENDPOINT");
  if (pub_endpoint.empty()) {
    string run_dir = "/run/hpx-radar-server";
    if (!dexist(run_dir)) {
      SPDLOG_ERROR(
          "Directory {} does not exist; it should be created when installing the HPx Radar Server. If"
          " running without installation, specify the ZeroMQ endpoint in environment variable HPXENDPOINT.",
          run_dir);
      return EXIT_NOTCONFIGURED;
    }
    pub_endpoint = "ipc://" + run_dir + "/hpx-pub.sock";
  }
  SPDLOG_INFO("Using ZeroMQ endpoint '{}'", pub_endpoint);
  return 0;
}
