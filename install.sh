#!/bin/bash
APP=hpx-radar-server  # Must have no spaces and be a valid file name.
DRIVER_NAMES=("Plx9056" "xdma")

log () {
    >&2 echo "install.sh: $@"
    echo "$@" | systemd-cat -t "${APP}--install.sh" -p "info"
}

logerr () {
    >&2 echo "install.sh ERROR: $@"
    echo "ERROR: $@" | systemd-cat -t "${APP}--install.sh" -p "err"
}

# Check if root
if [ $EUID -ne 0 ]; then
    logerr "Must be run as root!"
    exit 1
fi

# Get driver name from first arg
DRIVER_NAME_ERR="Must provide one of the following driver names: ${DRIVER_NAMES[@]}"
for name in ${DRIVER_NAMES[@]}; do
    if [ $name == "$1" ]; then
        DRIVER_NAME=$name
    fi
done
if [ -z "$DRIVER_NAME" ]; then
    logerr  $DRIVER_NAME_ERR
    exit 1
fi



# function debug
# {
#     echo "#############|  Entering DEBUG mode  |####################";
#     cmd=""
#     while [[ $cmd != "exit" ]]; do
#         read -p "> " cmd
#         case "$cmd" in
#             vars ) ( set -o posix ; set );;
#             exit ) ;;
#             * ) eval "$cmd";;
#         esac
#     done
#     echo "#############|  End of DEBUG mode |####################";
# }

# Set up variables
CONFIGPATH=/etc/$APP.toml
UNITFILE=/etc/systemd/system/$APP.service
# POLKITRULES=/usr/share/polkit-1/rules.d/57-manage-$APP.rules
SUDOERS=/etc/sudoers.d/$APP
TMPFILESCONFIG=/etc/tmpfiles.d/$APP.conf
if [ -z "$PREFIX" ]; then
    PREFIX=/usr/local
fi
INSTALLDIR="$PREFIX"/bin
SHAREDIR=$PREFIX/share/$APP
INSTALLVARSFILE=$SHAREDIR/install_vars
if [ -z $HPXUSER ]; then
    HPXUSER=hpxradar
fi
# By default HPXGROUP is the same as HPXUSER
if [ -z $HPXGROUP ]; then
    HPXGROUP=$HPXUSER
fi

# Make sure the binary has been built
if [ ! -e bin/${APP} ]; then
    logerr "The hpx-radar-server binary must be built first. Run \`make all\`."
    exit 2
fi

log "Trying uninstall first."
if [ -f $SHAREDIR/uninstall.sh ]; then
	if ! ( bash $SHAREDIR/uninstall.sh -C ); then
		logerr "Problem running uninstall.sh"
		# Continue anyway.
	fi
fi

log "Installing $APP to $INSTALLDIR/"
cp bin/$APP "$INSTALLDIR"/$APP

#  -- User & Group setup --
declare -a USERADDOPTS
UID_LIMS=(-K UID_MIN=500 -K UID_MAX=999)
GID_LIMS=(-K GID_MIN=500 -K GID_MAX=999)
USERADDOPTS=(--no-create-home --comment "User to run the $APP service" ${UID_LIMS[@]} ${GID_LIMS[@]})
if ! ( getent group $HPXGROUP > /dev/null ); then  # if group HPXGROUP does not exists
    if [ $HPXUSER != $HPXGROUP ]; then
        log "Creating group $HPXGROUP"
        if ! groupadd $HPXGROUP; then
            logerr "Failed creating group $HPXGROUP"
            exit 3
        fi
        USERADDOPTS+=(--gid $HPXGROUP)
    else
        USERADDOPTS+=(--user-group)
    fi
else
    log "Group $HPXGROUP already exists."
fi

if ! ( getent passwd $HPXUSER > /dev/null ); then  # if user HPXUSER does not exists...
    log "Creating user $HPXUSER with group $HPXGROUP"
    if ! useradd "${USERADDOPTS[@]}" $HPXUSER ; then
        logerr "Error creating user $HPXUSER"
        exit 4
    fi
else
    log "User $HPXUSER already exists."
fi
if ! id -nGz $HPXUSER | grep -qzxF $HPXGROUP ; then  # if HPXUSER does not belong to HPXGROUP...
    log "$HPXUSER is not a member of group $HPXGROUP (may need to log out if logged-in). Trying to add."
    if ! usermod -a -G $HPXGROUP $HPXUSER ; then
        logerr "Error adding $HPXUSER to group $HPXGROUP"
        exit 5
    fi
fi
log "NOTE: To allow users to control $APP and change the configuration, add them to group $HPXGROUP."
# -- End user & group setup --

log "Installing configuration file $CONFIGPATH"
declare -l OVERWRITE=y
if [ -f "$CONFIGPATH" ]; then
    log "$CONFIPATH already exists."
    log "  If the config file format has changed this may cause problems."
    log "  If overwrite is chosen a backup will be made first."
    log "  Overwrite? [Y/n]: "
    read -n 1 OVERWRITE
    echo
    OVERWRITE=${OVERWRITE:-"y"}  # set to "y" if empty
    if [ $OVERWRITE != "n" ]; then
        NOW=$(date -u +%Y%m%d%H%M%S)
        BACKUP_CONFIGPATH=${CONFIGPATH}-${NOW}.bak
        log "$CONFIGPATH will be backed-up to $BACKUP_CONFIGPATH"
        cp $CONFIGPATH $BACKUP_CONFIGPATH
    else
        log "Keeping existing $CONFIGPATH"
    fi
fi
if [ $OVERWRITE != "n" ]; then
    cp default_config.toml $CONFIGPATH
fi
chown $HPXUSER:$HPXGROUP $CONFIGPATH
chmod g+rw $CONFIGPATH

log "Creating/updating systemd service $UNITFILE"
if [ $DRIVER_NAME == "Plx9056" ] ; then
  REQUIRESAFTER="Requires=plx-driver-load.service
After=plx-driver-load.service"
fi

# -- start of heredoc --
cat << EOF > $UNITFILE
[Unit]
Description = HPx Radar Server
$REQUIRESAFTER

[Service]
Type = notify
NotifyAccess = main
ExecStart = "$INSTALLDIR/$APP" -d
ExecReload = /bin/kill -SIGHUP \$MAINPID
WatchdogSec = 2
TimeoutSec = 5
Restart = on-watchdog
User = $HPXUSER
Group = $HPXGROUP

[Install]
WantedBy = multi-user.target
EOF
# -- end of heredoc --

# Give group rw permissions
chgrp $HPXGROUP $UNITFILE
chmod g+rw $UNITFILE

# # This only worked on CentOS and Fedora. Do sudoers instead (below)
# log "Creating Polkit file to allow users of group $HPXGROUP to control $APP.service"
# # -- start of heredoc --
# cat << EOF > $POLKITRULES
# // Allow hpxradar to manage hpx-radar-server.service;
# // fall back to implicit authorization otherwise.
# polkit.addRule(function(action, subject) {
#     if (action.id == "org.freedesktop.systemd1.manage-units" &&
#         action.lookup("unit") == "hpx-radar-server.service" &&
#         subject.isInGroup("hpxradar")) {
#         return polkit.Result.YES;
#     }
# });
# EOF
# # -- end of heredoc

log "Creating /etc/sudoers.d/$APP" to allow $HPXGROUP group members to control the $APP.service with sudo.
# -- start of heredoc --
cat << EOF > $SUDOERS
%${HPXGROUP} ALL= NOPASSWD: /usr/bin/systemctl restart $APP.service
%${HPXGROUP} ALL= NOPASSWD: /usr/bin/systemctl start $APP.service
%${HPXGROUP} ALL= NOPASSWD: /usr/bin/systemctl stop $APP.service
%${HPXGROUP} ALL= NOPASSWD: /usr/bin/systemctl status $APP.service
EOF
# -- end of heredoc --
log "chmod 0440 $SUDOERS"
chmod 0440 $SUDOERS

log "Reloading systemd manager configuration."
systemctl daemon-reload

log "Creating tmpfiles.d configuration for socket folder and loading new rules."
# -- start of heredoc --
cat << EOF > $TMPFILESCONFIG
#Type Path        Mode User     Group     Age Argument
D     /run/$APP   2775 $HPXUSER $HPXGROUP
EOF
# -- end of heredoc
systemd-tmpfiles --prefix=/run/$APP --remove --create

log "Copying default config and uninstall scripts in $PREFIX/share/$APP"
mkdir -p $SHAREDIR
cp default_config.toml $SHAREDIR/
cp uninstall.sh $SHAREDIR/
chmod a+x $SHAREDIR/uninstall.sh
log "Saving install vars to $INSTALLVARSFILE"
echo -n > $INSTALLVARSFILE
echo "APP=$APP" >> $INSTALLVARSFILE
echo "CONFIGPATH=$CONFIGPATH" >> $INSTALLVARSFILE
echo "UNITFILE=$UNITFILE" >> $INSTALLVARSFILE
# echo "POLKITRULES=$POLKITRULES" >> $INSTALLVARSFILE
echo "SUDOERS=$SUDOERS" >> $INSTALLVARSFILE
echo "TMPFILESCONFIG=$TMPFILESCONFIG" >> $INSTALLVARSFILE
echo "PREFIX=$PREFIX" >> $INSTALLVARSFILE
echo "INSTALLDIR=$INSTALLDIR" >> $INSTALLVARSFILE
echo "HPXGROUP=$HPXGROUP" >> $INSTALLVARSFILE
echo "HPXUSER=$HPXUSER" >> $INSTALLVARSFILE
echo "SHAREDIR=$SHAREDIR" >> $INSTALLVARSFILE

log "Done."
