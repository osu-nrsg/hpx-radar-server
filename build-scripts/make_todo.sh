#!/bin/sh

# Check if leasot is installed. leasot is a tool to find TODO, FIXME etc. in
# source files. It can be installed with npm: npm install -g leasot
command -v leasot >/dev/null 2>&1 || { echo >&2 "leasot command missing. Will not update TODO.txt."; exit 0; }
echo "Output of \"leasot -x -A '.hpp,defaultParser' -T FUTURE src/**\":" > TODO.txt
leasot -x -A '.hpp,defaultParser' -T FUTURE src/** >> TODO.txt

