#!/bin/bash
APP=hpx-radar-server

log () {
	>&2 echo "uninstall.sh: $@"
	echo "$@" | systemd-cat -t "${APP}--uninstall.sh" -p info
}

logerr () {
	>&2 echo "uninstall.sh ERROR: $@"
	echo "ERROR: $@" | systemd-cat -t "${APP}--uninstall.sh" -p err
}

# Check if root
if [ $EUID -ne 0 ]; then
	logerr "Must be run as root!"
	exit 1
fi

KEEP_CONFIG=NO
while getopts ":C" options; do
  case "${options}" in
	  C)
			KEEP_CONFIG=YES
			;;
		*)
			exit 1
			;;
	esac
done

SOURCEDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
if [ ! -f $SOURCEDIR/install_vars ]; then
	logerr "install_vars file is missing from $SOURCEDIR. Cannot use uninstall.sh."
	exit 2
fi
log "Reading in install vars."
source $SOURCEDIR/install_vars

log "Stopping, disabling, and removing $APP.service"
if [ -n "$(systemctl list-unit-files | grep "^${APP}.service")" ] ; then
	if ! systemctl stop $APP.service; then
		logerr "Could not stop $APP.service"
		exit 3
	fi
	if [ $(systemctl is-enabled $APP.service) == "enabled" ]; then
		systemctl disable $APP.service
	fi
else
	log "$APP.service does not exist in systemd."
fi

rm $UNITFILE
systemctl daemon-reload

if [ -n "$POLKITRULES" ]; then
	log "Removing polkit rules file"
	rm $POLKITRULES
fi

if [ -n "$SUDOERS" ]; then
  log "Removing sudoers.d file"
	rm $SUDOERS
fi

log "Removing tmpfiles dir and tmpfiles.d config"
if [ -d /run/$APP ]; then
	# delete contents
	find /run/$APP/ -mindepth 1 -delete
	# try to delete with systemd-tmpfiles
	if ! systemd-tmpfiles --prefix=/run/$APP --remove ; then
		logerr "Problem removing tmpfiles dir with systemd-tmpfiles"
		# try manual remove
		if ! rmdir /run/$APP; then
			logerr "Could not remove tmpfiles dir"
			# No exit, still continue uninstalling
		fi
	fi
fi

rm $TMPFILESCONFIG


log "Removing binary"
rm $INSTALLDIR/$APP

log "Removing configuration and install files."
if [ $KEEP_CONFIG = "NO" ]; then
	rm $CONFIGPATH
else
	log "Keeping $CONFIGPATH"
fi
rm -r $SHAREDIR

log "Done."
