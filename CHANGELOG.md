# Changelog

All notable changes to the project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

- Fixes to `driver_setup.sh`

## [0.7.0] - 2021-05-18

- Add xdma driver support for HPx-410 card
- Adjust max range setting to get equal 3m range bins on both the HPx-200e and HPx-410.
- Add sudoers.d config to allow hpxradar users to control the service

## [0.6.0] - 2021-03-04

- #40 - Add tpg_mode to HPxConfig
- README corrections

## [0.5.0] - 2021-01-20

### Changed

- #36 - Transmit STATUS & CONFIG and send watchdog immediately on server startup.
- #37 - Clear alarm state after alarms have ceased.
- Added assertions to state machine to check for proper transitions
- See !34 for all commits.

## [0.4.0] - 2020-10-06

Completed milestone %"0.4.0".

### Added

- CHANGELOG.md

### Changed

- #26, #33 - signals handled immediately and signals are not lost due to state machine logic error
- #29, #34 - SIGCONT and SIGTSTP no longer handled. SIGHUP only restarts in daemon mode, otherwise quits.
- #30 - `hpx-radar-server.toml` config is only expected to be at `/etc/`, and not in `~/.config/`.
- #31 - Status and config send at proper interval now; also watchdog notification interval is independent
  from these once again.

## [0.3.0] - 2020-09-10

First Release. See [milestone 0.3](https://gitlab.com/osu-nrsg/hpx-daq/hpx-radar-server/-/milestones/2)
for issues.

[Unreleased]: https://gitlab.com/osu-nrsg/hpx-daq/hpx-radar-server/compare/v0.7.0...master
[0.7.0]: https://gitlab.com/osu-nrsg/hpx-daq/hpx-radar-server/compare/v0.6.0...v0.7.0
[0.6.0]: https://gitlab.com/osu-nrsg/hpx-daq/hpx-radar-server/compare/v0.5.0...v0.6.0
[0.5.0]: https://gitlab.com/osu-nrsg/hpx-daq/hpx-radar-server/compare/v0.4.0...v0.5.0
[0.4.0]: https://gitlab.com/osu-nrsg/hpx-daq/hpx-radar-server/compare/v0.3.0...v0.4.0
[0.3.0]: https://gitlab.com/osu-nrsg/hpx-daq/hpx-radar-server/-/tags/v0.3.0
