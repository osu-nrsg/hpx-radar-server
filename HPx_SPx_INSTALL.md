# Instructions for setting up the drivers and the SPx board support library for the HPx Radar Input Card <!-- omit in toc -->

***Note:** HPx Radar Server is only supported on Linux at this time. It should be possible to build on recent
(as of 2019) versions of most major distributions.*

## Contents <!-- omit in toc -->

- [Obtain and extract the SPx Board Support Library](#obtain-and-extract-the-spx-board-support-library)
- [Install Kernel Headers](#install-kernel-headers)
  - [RedHat/CentOS/Fedora](#redhatcentosfedora)
  - [Debian/Ubuntu/derivatives](#debianubuntuderivatives)
- [Driver Setup](#driver-setup)

## Obtain and extract the SPx Board Support Library

The SPx Board Support Library includes the actual SPx libraries and header files, example programs,
documentation, and the PLX driver source.

Acquire the the SPx board support library archive from the CD or download link provided by Cambridge Pixel
(e.g. `SPxBSLib-V1.83.tar.gz`) and extract its contents to `/opt/SPxBSLib`. For example:

```console
tar -xvf SPxBSLib-V1.83-linux.tar.gz
sudo cp -a SPxBSLib-V1.83 /opt/SPxBSLib
```

Or, to maintain the version:

```console
sudo tar -C /opt/ -xvf SPxBSLib-V1.83-linux.tar.gz
sudo ln -s /opt/SPxBSLib-V1.83 /opt/SPxBSLib
```

## Install Kernel Headers

To build kernel modules, the kernel headers must be available on the system, as well as C and Make build
tools.

### RedHat/CentOS/Fedora

```console
# dnf install gcc-c++ make kernel-devel kernel-headers
```

### Debian/Ubuntu/derivatives

```console
# apt install build-essential linux-headers-generic
```

## Driver Setup

The script [driver_setup.sh](driver_setup.sh) attempts to do the following:

1. Find the version of the driver (for the FPGA on the DAQ card) to use as the driver version
2. Either:
   1. Plx9056
      1. Create a driver source folder at `/usr/src/Plx9056-5.20`, where `5.20` is whatever driver version is
         found in the previous step, and copy the PLX SDK to that folder.
      2. Perform some small modifications to the included load/unload/build scripts.
      3. Create helper scripts to load, unload, and build the driver (kernel module).
      4. Create and enable a systemd service to automatically load the kernel module at boot.
   2. xdma
      1. Create driver source folder at `/usr/src/xdma-2018.3.50` where `2018.3.50` is the the driver
         version.
      2. Build and install the xdma driver.

As root, place it at `/opt/SPxBSLib` and execute it as root. For example:

```console
# cd /opt/SPxBSLib
# wget https://gitlab.com/osu-nrsg/hpx-daq/hpx-radar-server/-/raw/master/driver_setup.sh
# chmod +x ./driver_setup.sh
# # Either:
# ./driver_setup.sh Plx9056
# # --or--
# ./driver_setup.sh xdma
```
