Output of "leasot -x -A '.hpp,defaultParser' -T FUTURE src/**":

src/HPxServer.hpp
  line 71   TODO    Document the state machine.

src/RayHandler.cpp
  line 142  FUTURE  Add azimuth smoothing
  line 398  FUTURE  Alert clients? This is unlikely.

src/RayPool.hpp
  line 22   FUTURE  Consider replacing SpinLock with mutex

src/SPxErrorHandler.hpp
  line 18   FUTURE  Remove boost dependency

src/util/adaptors.hpp
  line 166  FUTURE  Switch to using std::chrono::time_point over ptime, or something else.

src/ZmqProxy.hpp
  line 10   FUTURE  Possibly re-make the proxy function itself to allow quitting by an atomic_bool?

 ✖ 7 todos/fixmes found
