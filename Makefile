#
# Makefile
#
#  Created on: Oct 23, 2019
#      Author: Randall Pittman <Randall.Pittman@oregonstate.edu>
#     License: MIT (see LICENSE.txt)
#
#  Based on example Makefile by Cambridge Pixel and generic makefile from
#    https://gist.github.com/maxtruxa/4b3929e118914ccef057f8a05c614b0f.
#

# Main program name
BIN := hpx-radar-server

# DEFAULTS
SPX ?= /opt/SPxBSLib
SPX_ARCH ?= x64

# arch stuff
ifeq ($(SPX_ARCH),x64)
	EXT = _64
else
	EXTRA_LIBS += -lirc
endif

# Dirs
SRCDIR := src
CONTRIBDIR := contrib
DEPDIR := .deps
TRUEBINDIR := bin
TRUEOBJDIR := obj
BUILDSDIR := builds
DEBUGDIR := $(BUILDSDIR)/debug
GPROFDIR := $(BUILDSDIR)/gprof
TESTDIR := tests
# For non-main builds, put obj and bin in builds/<buildname>
BUILDTARGET = $(filter debug gprof,$(MAKECMDGOALS))
ifeq ($(findstring debug,$(BUILDTARGET)), debug)
	DEPDIR := $(DEBUGDIR)
	OBJDIR := $(DEBUGDIR)
	BINDIR := $(DEBUGDIR)
	OPTIMIZEFLAG := -Og
else ifeq ($(findstring gprof,$(BUILDTARGET)), gprof)
	DEPDIR := $(GPROFDIR)
	OBJDIR := $(GPROFDIR)
	BINDIR := $(GPROFDIR)
	OPTIMIZEFLAG := -O3
else
	OBJDIR := $(TRUEOBJDIR)
	BINDIR := $(TRUEBINDIR)
	OPTIMIZEFLAG := -O3
	BUILDTARGET := main
endif

# Output program binary with path
APP = $(BINDIR)/$(BIN)

# Sources and objects
SRCS := $(wildcard $(SRCDIR)/*.cpp)
OBJS := $(SRCS:$(SRCDIR)/%.cpp=$(OBJDIR)/%.o)
DEPS := $(SRCS:$(SRCDIR)/%.cpp=$(DEPDIR)/%.d)
TESTSRCS := $(wildcard $(TESTDIR)/*.cpp)
TESTBINS := $(TESTSRCS:$(TESTDIR)/%.cpp=$(TESTDIR)/%.test)

# Objects that aren't the main app
SRCOBJS = $(filter-out $(OBJDIR)/$(BIN).o,$(OBJS))

# Tools
# Note, this is a c++ only app
CXX = g++
LD = g++
ifdef SPX_CC
	CXX = $(SPX_CC)
endif
AR = ar
RM = rm -f

## Flags and options
# Compiler options
CXXOPTIONS = -Wall -Wextra -std=c++17 $(OPTIMIZEFLAG)
# compile-time defines.
CXXDEFINES = $(SPX_CC_DEFS)
# Locations for header files
CXXINCLUDES = -I$(SPX)/Includes \
			  -I./$(SRCDIR) -I./$(CONTRIBDIR) \
			  $(SPX_CC_INCS)
# All options for compilation
CXXFLAGS += $(CXXOPTIONS) $(CXXDEFINES) $(CXXINCLUDES)
# Linker Flags
LDFLAGS += -L$(SPX)/Libs/linux
# Libs to link
LDLIBS += -lspx$(EXT) -lc -lstdc++ -lrt -lm -lpthread -lzmq -lmsgpackc -lboost_date_time -lspdlog -lsystemd
# flags for dependency generation
DEPFLAGS = -MT $@ -MD -MP -MF $(DEPDIR)/$*.Td

# building commands
COMPILE = $(CXX) $(DEPFLAGS) $(CXXFLAGS) -c
LINK = $(LD) -o $@ $^ $(LDFLAGS) $(SPX_LINK_OPTS) $(LDLIBS) $(SPX_CC_LIBS) $(EXTRA_LIBS)
PRECOMPILE =
POSTCOMPILE = mv -f $(DEPDIR)/$*.Td $(DEPDIR)/$*.d

# User targets
# .PHONY means that the target is not the name of a file.
# This prevents make from ignoring the rule if a file of that name exists.
.PHONY: all compile strtimes debug tests gprof clean cleantests cleanall install uninstall help todo init

all: $(APP)

compile: $(OBJS)

init:
	git config core.hooksPath .githooks

strtimes: CXXFLAGS += -DHPX_STR_TIMESTAMP
strtimes: $(APP)

debug: APP = $(DEBUGDIR)/$(BIN)
debug: CXXFLAGS += -g -ggdb
debug: $(APP)

tests: cleantests $(TESTBINS)

gprof: CXXFLAGS += -pg
gprof: LDFLAGS += -pg
gprof: APP = $(GPROFDIR)/$(BIN)
gprof: $(APP)

clean:
	$(RM) -r $(TRUEOBJDIR) $(DEPDIR) $(TRUEBINDIR)

cleandebug:
	$(RM) -r $(DEBUGDIR)

cleangprof:
	$(RM) -r $(GPROFDIR)

cleantests:
	$(RM) $(TESTBINS)


cleanall: clean cleantests
	$(RM) -r $(BUILDSDIR)

help:
	@echo Available targets: all init debug gprof clean cleantests help

# Internal targets
$(APP) : $(OBJS) $(SPX)/Libs/linux/libspx$(EXT).a
	@echo --$@--
	@mkdir -p $(BINDIR)
	$(LINK)

$(OBJDIR)/%.o: $(SRCDIR)/%.cpp
$(OBJDIR)/%.o: $(SRCDIR)/%.cpp $(DEPDIR)/%.d
	@echo --$@--
	@mkdir -p $(DEPDIR) > /dev/null
	@mkdir -p $(OBJDIR) > /dev/null
	$(PRECOMPILE)
	$(COMPILE) $< -o $@
	$(POSTCOMPILE)

# General rule for all test scripts in tests/
$(TESTDIR)/%.test: $(TESTDIR)/%.cpp $(OBJS)
	@echo --$@--
	@mkdir -p $(TESTDIR)
	$(CXX) $(CXXFLAGS) $< -o $@ $(SRCOBJS) $(LDFLAGS) $(LDLIBS)
	@echo $(BUILDTARGET) > $@.buildtarget


.PRECIOUS: $(DEPDIR)/%.d
$(DEPDIR)/%.d: ;

-include $(DEPS)
