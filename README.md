# README <!-- omit in toc -->

Doxygen docs at <https://osu-nrsg.gitlab.io/hpx-daq/hpx-radar-server>

## DISCLAIMERS <!-- omit in toc -->

This project is the sole property of Oregon State University, except as otherwise indicated (included
dependency libraries). Oregon State University is in no way affiliated with Cambridge Pixel Ltd. and
Cambridge Pixel Ltd. is under no obligation to support this software.

This project is still preliminary, subject to change, and is UNTESTED. It is
only made public now to allow for viewing by non-GitLab users.

## Contents <!-- omit in toc -->
<!-- Use VS Code Markdown All in One extension to auto-generate -->
- [Synopsis](#synopsis)
- [Motivation](#motivation)
- [Installation](#installation)
- [Running the Server](#running-the-server)
  - [Without Installation](#without-installation)
  - [With Installation](#with-installation)
- [Command Line Interface](#command-line-interface)
- [Configuration](#configuration)
- [Messages](#messages)
  - [ZeroMQ Message envelopes](#zeromq-message-envelopes)
  - [RAY message](#ray-message)
    - [Msgpack::map contents](#msgpackmap-contents)
  - [STATUS message](#status-message)
    - [STATUS Msgpack::map contents](#status-msgpackmap-contents)
  - [CONFIG message](#config-message)
    - [CONFIG Msgpack::map contents](#config-msgpackmap-contents)
- [Tests](#tests)
- [Contributors](#contributors)
- [License](#license)

## Synopsis

Radar signals captured with Cambridge Pixel HPx series radar interface cards may be accessed using a Linux or
Windows driver and C++ API. This project is a C++ program that is designed to be run as a
[new-style daemon](https://www.freedesktop.org/software/systemd/man/daemon.html#New-Style%20Daemons)
to constantly listen to the HPx card for new radar messages and then package each message with some header
data and send it to a ZeroMQ publisher socket for downstream processing by other apps.

## Motivation

While it would be possible to create a full radar recording and/or processing application in C++, this
 program is limited in scope. It configures the radar card, collects the radar messages, performs basic
 summing and timestamping operations, and publishes radar messages to clients. With the ZeroMQ pub/sub
 paradigm, multiple subscribers can access the messages simultaneously.

## Installation

See separate installation instructions at [INSTALL.md](INSTALL.md).

Uninstallation instructions are at the end of [INSTALL.md](INSTALL.md#uninstall).

## Running the Server

See the [Command Line Interface](#command-line-interface) section for CLI arguments.

### Without Installation

The radar server may be run without installation. From the directory where the software is built (e.g.
`/usr/local/src/hpx-radar-server`):

```console
# Specify the ZeroMQ endpoint to which the messages will be published
export HPXENDPOINT=ipc://WIMR.sock
# Launch the server with a config file
bin/hpx-radar-server -c default_config.toml
```

Is it necessary to specify `HPXENDPOINT` if the software has not been installed as in this case the default
socket location `/run/hpx-radar-server` will not exist. Similarly, no config file will have been installed at
`/etc/hpx-radar-server.toml` so one must be specified in the command line.

### With Installation

As noted in the [install script section of INSTALL.md](INSTALL.md#download-and-install-the-hpx-radar-server),
the installation script accomplishes the following:

- Binary executable copied to `/usr/local/bin/`
- Configuration file installed to `/etc/hpx-radar-server`
- Runtime socket directory created at `/run/hpx-radar-server`
- systemd service created
- Creation of `hpxradar` user and group for running the software

When HPx Radar Server is installed, the typical way of running the server is to start the systemd service,
and the typical way of configuring the server is to modfiy the `/etc/hpx-radar-server.toml` file.
Though these may be done by an administrator, the preferred way to do so is to add users to the `hpxradar`
group. (Remember to log-out and log-in again to obtain group membership after user `usermod`.)

Note that it is still possible to run the server from the command line as well, but it should now be callable
from anywhere, assuming `/usr/local/bin` is in your `PATH`. This may be useful for calling the service
with particular arguments for debugging purposes.

The systemd service unit file is located at `/etc/systemd/system/hpx-radar-server.service`, and may only be
modified by an administrator. It is configured to run `hpx-radar-server` in daemon mode. It will use the
config file at `/etc/hpx-radar-server.toml` and open the ZeroMQ socket at
`/run/hpx-radar-server/hpx-pub.sock` which may be accessed by any member of the `hpxradar` group.

The `WatchdogSec` value in the service unit file specifies the maximum period of watchdog notifications sent
by the service before systemd will assume the service has died. If it has died it will attempt to restart it.
`hpx-radar-server` will automatically configure itself to send watchdog notifications at half this period
(twice as frequently).

To start the service, run `systemctl start hpx-radar-server`.

To stop the service, run `systemctl stop hpx-radar-server`.

The service status may be checked with `systemctl status hpx-radar-server` or
`journalctl -e -u hpx-radar-server`.

To cause the server to start at boot, run `systemctl enable hpx-radar-server`. This may be disabled with
`systemctl disable hpx-radar-server`.

To reload the configuration file and continue running, run `systemctl restart hpx-radar-server`.

## Command Line Interface

The following help text from running `hpx-radar-server -h` provides information on the command-line
options.

```text
Start the HPx Radar Server.

USAGE:

  hpx-radar-server [-c config_file] [-t config_toml] [-d] [-v level] [-V] [-h]

Note: By default the server will use the ipc:// protocol and attempt to set
up unix domain socket at /run/hpx-radar-server/hpx-pub.sock for the ZeroMQ
publisher. To specify a different ZeroMQ endpoint, set it in the environment
variable HPXENDPOINT.

OPTIONS:
 -c CONFIG_FILE
    Path to a TOML-formatted configuration file with the server's
    configuration. If not specified, $HOME/.config/hpx-radar-server.toml or
    /etc/hpx-radar-server.toml is used, if available (in that order of
    preference).
 -t "CONFIG_TOML"
    Optional TOML-formatted string containing command-line overrides of the
    values in the config file.
 -d
    Run as (new-style) daemon. Log content will be written to journald (syslog)
    rather than console. Config may be reloaded by using
      systemctl restart hpx-radar-server
    systemd will be notified of process state and status with `sd_notify`.
 -v LEVEL
    Set log level to LEVEL.
    Log levels are 0:trace 1:debug 2:info 3:warning 4:error 5:critical 6:off
    Default level is 2 for daemon, 1 for non-daemon.
 -V
    Print version to console and exit.
 -h
    Print this message.

EXAMPLES
  # Look for hpx-radar-server.toml config file in /etc or ~/.config, and set
  # verbosity level to TRACE.
  hpx-radar-server -v 0

  # Enable daemon mode and use the config file at configs/test_config.toml
  hpx-radar-server -d -c configs/test_config.toml

  # Apply some modifications to the config (TOML is newline-delimited):
  CONFIG="
  TPG.enabled = true
  TPG.trigger_frequency = 3000"
  hpx-radar-server -c configs/test_config.toml -t "$CONFIG"

  # Print version number and exit:
  hpx-radar-server -V

  # Print this text and exit:
  hpx-radar-server -h
```

The systemd service unit file `/etc/systemd/system/hpx-radar-server.service` defaults to simply running
`hpx-radar-server -d`. This may be modified, but doing so will require administrative privileges.

## Configuration

The configuration file format for the hpx-radar-server is
 [TOML](https://github.com/toml-lang/toml), an `ini`-like syntax. The following table shows the
 configuration parameters and their TOML names. Also see
 [default_config.toml](default_config.toml).

| Section.Key Name                  | Type   | HPxConfig variable           | Default    | Description |
| :---------------                  | :---   | :-----------------           | :------    | :---------- |
| Board.board_idx                   | uint   | board_idx                    | 0             | Index of HPx card to use, if more than one installed. |
| Board.analog_channel              | char   | analog_channel               | A             | Which analog video channel of the HPx card to use (only one supported at present) |
| Sampling.num_samples              | uint   | num_samples                  | 1024          | How many range samples to record for each ray |
| Sampling.start_range_m            | double | start_range_m                | 0             | Range at which to collect the first sample of the ray |
| Sampling.range_correction_m       | double | range_correction_metres      | 0             | Offset of trigger time to range 0, in meters. |
| Leveling.voltage_offset           | double | voltage_offset               | 0             | Voltage offset to add to sampling level 0. (e.g. if signal is -250 mV to 1V, set to 0.25) |
| Leveling.gain                     | double | gain                         | 1             | (Digital) multiplier applied to signal level |
| TPG.enabled                       | bool   | tpg_enabled                  | false         | Enable the test pattern generator |
| TPG.mode                          | string | tpg_mode                     | "analog_ramp" | Mode for the test pattern generator. Options: "analog_ramp", "digital_pulse", "digital_ramp", "analog_dac" |
| TPG.trigger_frequency             | double | tpg_trigger_frequency        | 2000          | Test pattern generator trigger frequency (PRF) |
| TPG.acp_frequency                 | double | tpg_acp_frequency            | 2560          | Time-frequency of azimuth pulses |
| TPG.arp_frequency                 | uint   | tpg_arp_frequency            | 2048          | Number of azimuth pulses between each heading pulse |
| Misc.enable_azimuth_interpolation | bool   | enable_azimuth_interpolation | true          | Interpolate the azimuth angle value for triggers between azimuth pulses |
| Misc.status_and_config_period_ms  | uint   | status_and_config_period_ms  | 500           | Period of status & config messages, in ms. |
| Misc.calc_true_timestamps         | bool   | calc_true_timestamps         | true          | If true: RAY `timestamp` is calculated to the true time the ray was received. |
| Misc.summing_factor               | uint   | summing_factor               | 1             | If > 1, this many consecutive rays are summed together for each ray sent out. Must be in range [1, 16]. |
| Buffers.spokes_per_interrrupt     | int    | spokes_per_interrupt         | -1 (unset)    | Number of rays (spokes) between swapping ray buffers on the card |
| Buffers.interrupts_per_second     | int    | interrupts_per_second        | -1 (unset)    | Number of times to swap ray buffers on the card per second |
| Filtering.start_azival            | uint   | start_azival                 | 0             | Minimum azimuth value of published rays (0..65535) |
| Filtering.end_azival              | uint   | end_azival                   | 0             | Maximum azimuth value of published rays (0..65535), exclusive. If less than start\_azival, then the azi range crosses 0. If equal to start\_azival, then no azimuths are filtered. |
| Filtering.send_filtered           | bool   | send_filtered                | true          | If false, azimuth-filtered messages will not be sent. Otherwise they have data len 2 `0xffff`. |

## Messages

The hpx-radar-server application publishes a number of messages to the specified endpoint
 (tcp, ipc, etc.) using the [ZeroMQ PUB/SUB pattern with
 envelopes](http://zguide.zeromq.org/page:all#Pub-Sub-Message-Envelopes) with a key defining the
 message type and a body. If the message body contains
 multiple data items, they are packed into a [MessagePack](https://msgpack.org/)
 [map](https://github.com/msgpack/msgpack/blob/master/spec.md#map-format-family). These are the
 currently defined message types:

### ZeroMQ Message envelopes

| Key                       | Body         |
|:---                       |:----         |
| [RAY](#ray-message)       | msgpack::map |
| [STATUS](#status-message) | msgpack::map |
| [CONFIG](#config-message) | msgpack::map |

### RAY message

Every time a radar return/ray is received by the HPx DAQ card, it is processed and packed into a
 RAY message, so these messages will be published, on average, at same rate as the radar's PRF.
 However, since the DAQ card switches back and forth between two ray buffers, the messages will
 be processed and therefore published in bursts sized by the *spokes_per_interrupt* or
 *interrupts_per_second* settings. Note that if `Misc.calc_true_timestamps` is `true`, then each bank of
 rays will not be sent out until the next bank begins processing.

#### Msgpack::map contents

| Key           | Type      | Description |
|:---           |:----      |:----------- |
| rotation_idx  | uint      | Number of rotations completed since start of server |
| ray_idx       | uint      | Index of the ray in the rotation |
| memory_bank   | uint      | Which DAQ buffer is currently being read from. |
| proc_time     | Timestamp | UTC time the processing of this ray began |
| time_interval | uint      | Microseconds since last return (from DAQ card) |
| calc\_time    | Timestamp | Calulated "true time" the ray was received, if `Misc.calc_true_timestamps` is `true`. |
| azimuth       | uint      | Azimuth count CW from heading 0..65535 |
| data          | bin       | Uint16 array of ray intensity values. 12-bit (0-4095). `0xffff` signifies a filtered ray. |

Notes:

- The total packed size depends on the packing type used for each value, which varies with the size
 of the value. Nevertheless, $150 + 2 \times num\_samples$ is a good starting point for unpacking
 buffer size.
- Prior to version 0.2.0, the time fields were ISO8601 strings (*YYYY-MM-DDTHH:MM:SS.ffffffZ*) but
 they now are packed as MessagePack Timestamps (`ext` type code -1).
 [See the MessagPack spec for details.](https://github.com/msgpack/msgpack/blob/master/spec.md#timestamp-extension-type).
 To return to packing ISO8601 strings, compile with the `-DHPX_STR_TIMESTAMP` flag.

### STATUS message

The STATUS message contains some basic information about the status of the server.
This may be expanded in the future. Period determined by `Misc.status_and_config_period_ms`.

#### STATUS Msgpack::map contents

| Key           | Type   | Description |
|:---           |:----   |:----------- |
| state         | int    | Enumerated states: [`begin = 0, config_and_start, running, running_alarm, stopping, stopped, stopped_config_error, stopped_run_error, end`]<sup>1</sup><sup>2</sup> |
| avg_prf       | double | AvgPRFInstantaneous (see HPx-200 manual) |
| meas_azimuths | uint   | Number of ACPs in the last rotation |
| meas_rays     | uint   | Number of rays in the last rotation (not summed) |
| runtime_s     | uint   | Number of seconds that the server has been running |
| message       | string | Message for error or degraded states. |

<sup>1. See ServerState.hpp.</sup>

<sup>2. The `running_alarm` state indicates that there is an ACP, ARP, or trigger alarm. The `stopped` state
is reserved for future use.</sup>

### CONFIG message

The CONFIG message contains both manually-set configuration parameters as well as calculated or
 measured values retrieved the card. Many of these are the same as set in the config file. Period determined
 by `Misc.status_and_config_period_ms`.

#### CONFIG Msgpack::map contents

| Key                          | Type   | Description |
|:---                          |:----   |:----------- |
| board_idx                    | uint   | HPx card index on the OS |
| analog_channel               | char   | Analog channel chosen for acquisition (A or B) |
| num_samples                  | uint   | Number of samples to acquire for each ray |
| start_range_metres           | double | Range at which to collect the first sample of the ray |
| range_correction_metres      | double | Offset of trigger time to range 0, in meters. |
| voltage_offset               | double | Voltage offset to add to sampling level 0. (e.g. if signal is -250 mV to 1V, set to 0.25) |
| gain                         | double | (Digital) multiplier applied to signal level |
| tpg_enabled                  | bool   | Is the test pattern generator enabled |
| tpg_mode                     | uint   | Enumerated TPG modes: `analog_ramp = 0, digital_pulse = 1, digital_ramp = 2, analog_dac = 3` |
| tpg_trigger_frequency        | double | Test pattern generator trigger frequency (PRF) |
| tpg_acp_frequency            | double | Time-frequency of azimuth pulses |
| tpg_arp_frequency            | uint   | Number of azimuth pulses between each heading pulse |
| enable_azimuth_interpolation | bool   | Is azimuith interpolation enabled |
| status_and_config_period_ms  | uint   | Period of status & config messages, in ms |
| spokes_per_interrupt         | int    | Number of rays (spokes) between swapping ray buffers on the card (-1 unset) |
| interrupts_per_second        | int    | Number of times to swap ray buffers on the card per second (-1 unset) |
| calc_true_timestamps         | bool   | If true: RAY `timestamp` is calculated to the true time the ray was received. |
| summing_factor               | uint   | If > 1, this many rays are summed together for each ray sent out. |
| start_azival                 | uint   | Minimum azimuth value of published rays (0..65535) |
| end_azival                   | uint   | Maximum azimuth value of published rays (0..65535), exclusive. If less than start\_azival, then the azi range crosses 0. If equal to start\_azival, then no azimuths are filtered. |
| send_filtered                | bool   | If false, azimuth-filtered messages will not be sent. Otherwise they have data len 2 `0xffff`. |
| card_name                    | string | Name of the HPx card, as retrieved in software |
| fpga_version                 | uint   | Current FPGA gateware version |
| calc_start_range_metres      | double | Calculated true start range related to *start_range_metres* and *range_correction_metres* |
| calc_end_range_metres        | double | Calculated true end range based on calculated start range and number of samples recorded. |
| meas_spokes_per_interrupt    | uint   | Current number of spokes being buffered up between interrupts |
| version                      | string | Current software version. E.g. "0.2.0" |

## Tests

TBD

## Contributors

At this time, this is a project solely for members of the Nearshore Remote
Sensing Group at Oregon State University.

## License

This project is licensed under the MIT license. See LICENSE.txt.
Dependency licenses:

- Cambridge Pixel SPx Development Library - Proprietary/Closed Source
- GCC stdlib - GPLv3 with the GCC Runtime Library Exception
- libzmq - LGPLv3/MPL2
- cppzmq - MIT
- boost etc. - Boost Software License
- msgpack-c - Boost Software License
- cpptoml - MIT
- spdlog - MIT
