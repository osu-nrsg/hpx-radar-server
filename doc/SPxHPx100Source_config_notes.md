Notes on SPxHPx100Source Configurations Parameters
# Purpose
This document is intended to supplement the Cambridge Pixel SPx HPx-200 User Manual (v1.72 at the time of writing, Cambridge Pixel document number CP-25-161-02). The notes contained herein are provided to explain how the Oregon State University hpx-radar-server software configures the HPx data acquisition card.
Sections 2, 3, and 4 of this document correspond directly to sections 4, 5, and 6 of the HPx-200 User Manual, such that subsection numbers for configuration parameters match one another. Any sections in which the configuration parameter(s) are *not set* is marked *not used*.
Parameters that should be provided by the input configuration are marked with *config*.
`Get` functions are for the most part not listed.
# Standard API
## Construction and Destruction
### `SPxHPx100Source(SPxRIB *buffer);`
* We always supply the `SPxHPx100Source` constructor with `NULL` as the radar input buffer argument because we are not using the full SPx processing suite, but rather using the “Board Support Library” approach.
### `~SPxHPx100Source;`
* In the hpx-radar-server app, the `SPxHPx100Source` object is created and destroyed automatically (statically allocated, rather than dynamically with `new`), so we don’t need to explicitly call the destructor. However, it is important to note that any static variables in the radar data handling function (see section 4.16) last for the lifetime of the `SPxHPx100Source` object.

## Opening and Closing Boards
### `OpenBoard(unsigned int idx=0, int noInit=False);` - *config*
We only have a single card on our system and only open the board once, so we call with no arguments (accept both defaults).
### `CloseBoard(int noStop=FALSE);`
* Always `noStop=FALSE`.
### `ProbeBoard(unsigned int idx=0);` - *not used*
## Selecting Video Channels
### `SetVideoChannel(unsigned int video);` - hard set
* Set to SPX_HPX_200_VIDEO_A_12BIT to acquire sample data with 12-bit resolution from channel A of the card.

## Sampling Range
### `SetNumSamples(unsigned int numSamples);` - *config*
* We set this to the number of samples we want to collect per ray (each trigger), starting at `StartRangeMetres`.
### `SetStartRangeMetres(double metres);` - *config*
* Set the minimum range, in meters to start collecting data. Zero if *not set*.
* Note British spelling of 'metres'.
### `GetStartRangeMetres()`
* We use this to find out the *actual* start range. Probably rounded to the nearedest range bin boundary.
### `SetEndRangeMetres(double metres);` - *not set*
* This can be used to record a large range subsampled to `NumSamples`. We leave this unset, which ensures that `EndRange` is always $`range_{start} + n_{samples}f_{sampling}\frac{c_{0}}{2}`$
### `GetEndRangeMetres();`
* We can use to get the true calculated end range.
### `SetEndRangePulseEnable(unsigned int enable);` - *not set*
### `SetRangeCorrectionMetres(double metres);` - *config*
* We set this value to ensure that the range is correct. This value is typically determined by the radar model, and relates to the time difference between when the trigger signal is detected by the DAQ card and when the RF pulse is actually transmitted by the radar.
### `SetEndRangeAutoFactor(double factor);` - *not set*
### `SetEndRangeAutoRounding(double metres);` - *not set*
### `SetAvgPRFIntervalMsecs(UINT32 msecs);` - *not set*
### `SetAvgPRFChangeThresholdPercent(double percent);` - *not set*

# Advanced API
## Analogue Offset
### `SetOffsetA(double voltage);` - *config*
* This is the voltage level we want the DAQ to digitize to 0.
### `SetOffsetB(double voltage);` - *not set*

## Gain Control
### `SetGainA(double gain);` - *config*
* Digital gain multiplier for channel A. We usually leave this at 1.0 and normalize the signal in post-processing.
### `SetGainB(double gain);` - *not set*
### `StartAutoGainOffset();` - *not used*
## LUT Configuration - *not used*
### LUT Loading - *not used*
### LUT Utility Functions - *not used*
### LUT-D Masks - *not used*
### LUT Write Mask - *not used*
## General Purpose I/O Ports - *not used*
## Azimuth Resolution
In general, we trust the HPx card to automatically calculate the number of azimuths per rotation. We use `GetMeasAzimuths()` to get the value determined by the card. In the future we could use `SetNumAzimuths` to hard-set the expected number of azimuths if auto detection wasn't working. `SetNumAzimuths` is not used for the test pattern generator. 
## Scan Mode and Direction - *not used*
## Azimuth Reference - *not used*
## Test Pattern Generator
### `SetTPG(unsigned int tpg);` - *config*
* When testing with no radar, we set this to `SPX_HPX100_TPG_ANALOGUE_A`.
* When not in use, this can be left unset or set to `SPX_HPX100_TPG_DISABLE`
### `SetTPGtrgFreq(double hz);` - *config*
* PRF of test pattern generator.
### `SetTPGacpFreq(double hz);` - *config*
* Frequency of azimuth pulses (ACPs) for the test pattern generator. Azimuth angle is determine by this rate with the ARP rate.
### `SetTPGarpFreq(unsigned int numAzis);` - *config*
* Frequency of heading pulses for the test pattern generator, in number of azimuths
## ACP/ARP/TRG Control - *not used*
* For now these are left to defaults and not configured - not needed for typical marine radars
### Inverted Signals - *not used*
* For now these are left to defaults and not configured - not needed for typical marine radars
### Azimuth Interpolation
#### `SetAziInterpolation(int enable);` - *config*
The ACP resolution of marine radars is not typically very high, so this will be enabled in most cases.
### Programmable thresholds - *not used*
### De-bounce - *not used*
* Doesn't seem to be needed for now.
## SIG1/SIG2 Control - *not used*
## Digital Inputs - *not used*
## Interrupt Rate/Latency
I've thought quite a bit about these settings, and unfortunately haven't come up with what I think is the best answer for how to use them, if at all. They are potentially important when it comes to putting accurate timestamps on the rays recorded by the HPx card. See [[Reports/HPx Card Time Reporting]] for my investigation. Probably the best bet is to use the `SetSpokesPerInterrup` over `SetInterruptsPerSecond`. We never really collect more than 4096 range samples. So with 4096 samples, each return would be 2*4096 + 12 = 8204 bytes. If each buffer on the DAQ card is 2MB, then the maximum number of spokes per interrupt would be 255. With a PRF of 2000 Hz, this would result in 7.8 interrupts per second, or one interrupt every 128 ms. To get close to an even number of interrupts per second, we can work backwards: 8 interrupts per second >> 250 spokes per interrupt.
### `SetSpokesPerInterrupt(unsigned int numSpokes);` - *optional config*
* Probably will set this to some constant.
### `SetInterruptsPerSecond(unsigned int numInts);` - *optional config*
* This will most likely remain unset. If it is set, it overrides `SetSpokesPerInterrupt`. This will be an optional config parameter that will not be set if not specified.

## Status Monitoring
* Might use some of these for reporting status. TBD.
* Just noticed the GetBank() value, which might be able to be used when processing returns to help with timestamping.
## Alarm Monitoring
* I think I'll probably use the `InstallAlarmFn` to publish alarms to clients.
## Built In Test - *not used*
## Direct Data Access
### `InstallDataFn(...);`
* Install the ray_handler_fn.
## Data Packing Indicator - *not used*
## Remote Parameter Control - *not used*
# Debug and Diagnostics - *not used*
## Card Type, Name and Location
## FPGA Version
## Debug Options
## DMA Control
## Low-Level Hardware Access
## Statistics Gathering