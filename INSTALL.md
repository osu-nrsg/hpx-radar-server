# How to install the HPx Radar Server <!-- omit in toc -->

***Note:** HPx Radar Server is only supported on Linux at this time. It should be possible to build on recent (as of 2019) versions of most major distributions.*

## Contents <!-- omit in toc -->

- [Install the HPx card driver and board support library](#install-the-hpx-card-driver-and-board-support-library)
- [Acquire and install dependencies](#acquire-and-install-dependencies)
  - [Install build tools](#install-build-tools)
  - [Give all permissions at `/usr/local/src`](#give-all-permissions-at-usrlocalsrc)
  - [Download and install dependencies](#download-and-install-dependencies)
    - [msgpack-c](#msgpack-c)
    - [spdlog](#spdlog)
    - [cpptoml](#cpptoml)
- [Download and install the HPx Radar Server](#download-and-install-the-hpx-radar-server)
- [Add users to the `hpxradar` group](#add-users-to-the-hpxradar-group)
- [Manage the hpx-radar-server service](#manage-the-hpx-radar-server-service)
  - [Enable the service to start at boot](#enable-the-service-to-start-at-boot)
  - [Stop the the service from starting at boot](#stop-the-the-service-from-starting-at-boot)
  - [Start the service](#start-the-service)
  - [Stop the service](#stop-the-service)
  - [Restart the service (reloads the configuration from `/etc/hpx-radar-server.toml`)](#restart-the-service-reloads-the-configuration-from-etchpx-radar-servertoml)
  - [Get status of the service](#get-status-of-the-service)
  - [See logs of the service (`-e` scrolls to the end, or most recent)](#see-logs-of-the-service--e-scrolls-to-the-end-or-most-recent)
  - [Follow the service log (like `tail -f`)](#follow-the-service-log-like-tail--f)
- [Uninstall](#uninstall)

## Install the HPx card driver and board support library

See instructions in [HPx_SPx_INSTALL.md](HPx_SPx_INSTALL.md). Note that the board support library must be installed to
`/opt/SPxBSLib` in order for the HPx Radar Server to compile and build.

## Acquire and install dependencies

### Install build tools

Using your system's package manager, install the following packages. If the indicated version is not
available in your package manager, install from source to `/usr/local`.

- cmake
- g++ (e.g. `gcc-c++`)
- make
- systemd development library (e.g. `systemd-devel`)
- Boost development library v1.31 or later (e.g. `boost-devel`)
- Boost Date-time library v1.31 or later (e.g. `boost-date-time`)
- The cppzmq (C++ ZeroMQ) development library v4.6 or later (e.g. `cppzmq-devel`)

### Give all permissions at `/usr/local/src`

The `/usr/local/src` directory is where we will build software. This is typically only writable by root, but
there's no harm in making it writable by everyone, as it's just where we will build software that will be
installed elsewhere.

```console
# # make sure it exists
# sudo mkdir -p /usr/local/src
# # Allow everyone to read/write/execute the dir
# sudo chmod a+rwx /usr/local/src
```

### Download and install dependencies

As of Sept. 2020 the following 3rd-party libraries were not available in up-to-date versions in OS
repositories and thus were installed from source. Continue for specific build instructions.

- msgpack-c (C/C++ MessagePack library) v3.2.1 or later
- spdlog (C++ Logging library) v1.8.0 or later
- cpptoml (C++ TOML library) v0.1.1 [N.B. Soon migrating to ToruNiina/toml11]

[Linux Mint 20 install packages and cmake args](https://gitlab.com/-/snippets/2055946)

The below instructions are more-or-less the same for each software package: Download the package, extract the
source to `/usr/local/src`, then build and install the package using CMake and Make.

To uninstall any of these CMake-built packages, traverse to the `build` folder created in the install
process and execute the following:

```console
sudo xargs rm < install_manifest.txt  # removes all files that were installed, listed in install_manifest.txt
```

#### msgpack-c

***Note additional cmake argument to disable building shared libraries (static-only).***

```console
cd /usr/local/src
# Download release 3.2.1 or greater
wget https://github.com/msgpack/msgpack-c/releases/download/cpp-3.3.0/msgpack-3.3.0.tar.gz
tar -xvf msgpack-3.3.0.tar.gz
cd msgpack-3.3.0
mkdir build && cd build
cmake -DCMAKE_INSTALL_PREFIX=/usr/local -DMSGPACK_ENABLE_SHARED=OFF ..
make -j
sudo make install
```

#### spdlog

```console
cd /usr/local/src
# Download release 1.8.0 or greater
wget https://github.com/gabime/spdlog/archive/v1.8.0.tar.gz -O spdlog_1.8.0.tar.gz
tar -xvf spdlog_1.8.0.tar.gz
cd spdlog-1.8.0
mkdir build && cd build
cmake -DCMAKE_INSTALL_PREFIX=/usr/local ..
make -j
sudo make install
```

#### cpptoml

```console
cd /usr/local/src
# Download release 0.1.1 or greater
wget https://github.com/skystrife/cpptoml/archive/v0.1.1.tar.gz -O cpptoml_0.1.1.tar.gz
tar -xvf cpptoml_0.1.1.tar.gz
cd cpptoml-0.1.1
mkdir build && cd build
cmake -DCMAKE_INSTALL_PREFIX=/usr/local ..
make -j
sudo make install
```

## Download and install the HPx Radar Server

```console
cd /usr/local/src
# Clone the main branch of the git repository.
# (Or download a release from the Releases page)
git clone https://gitlab.com/osu-nrsg/hpx-daq/hpx-radar-server.git
cd hpx-radar-server
make -j
# Run one of the following:
# Install and expect the Plx9056 driver to be loaded
./install.sh Plx9056
# Install and expect the xdma driver to be loaded
./install.sh xdma
```

`make install` runs `install.sh`, which does the following:

- Copy the hpx-radar-server executable to `/usr/local/bin/`
- Set up the user and group that can run and control the service, respectively.
- Install the config file at `/etc/hpx-radar-server.toml` (prompt if already present)
- Create systemd service `hpx-radar-server.service` to run the server as a service
- Create a Polkit file to allow members of the `hpxradar` group to control the service
- Create a `tmpfiles.d` config file to create a `/run/hpx-radar-server` folder at boot where the ZeroMQ
socket can be set up a runtime.
- Save install info and uninstall script to `/usr/local/share/hpx-radar-server`.

Note, at any time the default configuration may be restored by running the following:

```console
cp /usr/local/share/hpx-radar-server/default_config.toml /etc/hpx-radar-server.toml
```

## Add users to the `hpxradar` group

The `hpxradar` unprivileged user and group are created when `install.sh` runs (set `HPXUSER` and/or
`HPXGROUP` environment variables to override). Any users added to the `hpxradar` group are allowed to
control the hpx-radar-server systemd service and may edit the configuration at `/etc/hpx-radar-server.toml`.

```console
# Add user daq-user to the hpxradar group
sudo usermod -a -G hpxradar daq-user
```

Note that group membership is enacted at login, so any logged-in users must log out and back in before they
will be included in the `hpxradar` group.

## Manage the hpx-radar-server service

Members of the `hpxradar` group may manager the service:

### Enable the service to start at boot

```console
sudo systemctl enable hpx-radar-server.service
```

### Stop the the service from starting at boot

```console
sudo systemctl disable hpx-radar-server.service
```

### Start the service

```console
sudo systemctl start hpx-radar-server.service
```

### Stop the service

```console
sudo systemctl stop hpx-radar-server.service
```

### Restart the service (reloads the configuration from `/etc/hpx-radar-server.toml`)

```console
sudo systemctl restart hpx-radar-server.service
```

### Get status of the service

```console
systemctl status hpx-radar-server.service
```

### See logs of the service (`-e` scrolls to the end, or most recent)

```console
journalctl -e -u hpx-radar-server.service
```

### Follow the service log (like `tail -f`)

```console
journalctl -f -u hpx-radar-server.service
```

See `man journalctl` for more log options

## Uninstall

The `install.sh` script adds some files to `/usr/local/share/hpx-radar-server/`, including an uninstall
script. Run this to uninstall the server.

```console
$ cd /usr/local/share/hpx-radar-server
$ sudo ./uninstall.sh
uninstall.sh: Reading in install vars.
uninstall.sh: Stopping, disabling, and removing hpx-radar-server.service
uninstall.sh: Removing sudoers.d file
uninstall.sh: Removing tmpfiles dir and tmpfiles.d config
uninstall.sh: Removing binary
uninstall.sh: Removing configuration and install files.
uninstall.sh: Done.
```
