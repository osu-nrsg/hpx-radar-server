#!/bin/bash

# Retrieve version string; based on version.sh from libzmq.

#
# This script extracts the HPx Radar Server version from src/version.h, which is the master
# location for this information.
#

VER_PATH=src/version.h

if [ ! -f "$VER_PATH" ]; then
    echo "version.sh: error: $VER_PATH does not exist" 1>&2
    exit 1
fi
MAJOR=$(egrep '^#define +HPXRADARSERVER_VERSION_MAJOR +[0-9]+$' "$VER_PATH")
MINOR=$(egrep '^#define +HPXRADARSERVER_VERSION_MINOR +[0-9]+$' "$VER_PATH")
PATCH=$(egrep '^#define +HPXRADARSERVER_VERSION_REVISION +[0-9]+$' "$VER_PATH")
if [ -z "$MAJOR" -o -z "$MINOR" -o -z "$PATCH" ]; then
    echo "version.sh: error: could not extract version from \"$VER_PATH\"" 1>&2
    exit 1
fi
MAJOR=$(echo $MAJOR | awk '{ print $3 }')
MINOR=$(echo $MINOR | awk '{ print $3 }')
PATCH=$(echo $PATCH | awk '{ print $3 }')
echo $MAJOR.$MINOR.$PATCH | tr -d '\n'
